@servers(['web' => 'dengue-deploy'])

<?php
$repo = 'git@gitlab.com:alphadengue/moduloweb.git';
$release_dir = '/var/www/AlphaDengue/releases';
$app_dir = '/var/www/AlphaDengue/current';
$shared_dir = '/var/www/AlphaDengue/shared';
$release = 'release_' . date('YmdHis');
?>

@macro('deploy', ['on' => 'web'])
    fetch_repo
    run_composer
    update_permissions
    update_symlinks
@endmacro

@task('fetch_repo')
    echo 'Fetching repository';
    [ -d {{ $release_dir }} ] || mkdir -p {{ $release_dir }};
    cd {{ $release_dir }};
    git clone {{ $repo }} {{ $release }}
@endtask

@task('run_composer')
    echo 'Running composer'
    cd {{ $release_dir }}/{{ $release }}
    composer install --prefer-dist
    ln -s {{ $shared_dir }}/.env ./.env
    yarn install
    gulp
@endtask

@task('update_permissions')
    echo 'Updating permissions';
    cd {{ $release_dir }};
    sudo chown -R ubuntu:www-data {{ $release }};
    sudo chmod -R ug+rwx {{ $release }};
@endtask

@task('update_symlinks')
    echo 'Updating symlinks';
    ln -nfs {{ $release_dir }}/{{ $release }} {{ $app_dir }}
    sudo chown -Rh ubuntu:www-data {{ $app_dir }}
    sudo service php7.0-fpm restart
@endtask
