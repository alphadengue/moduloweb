<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_id',
        'street_number',
        'additional_address_details',
        'building_type_id',
        'latitude',
        'longitude'
    ];

    /**
     * Retrieves the street of this building
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function street()
    {
        return $this->belongsTo(Street::class);
    }

    /**
     * Retrieves the type of this building
     * FIXME: Change the method name
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building_type()
    {
        return $this->belongsTo(BuildingType::class);
    }

    /**
     * Retrieves the visits of this building
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visits()
    {
        return $this->hasMany(Visit::class)->orderBy('visited_at');
    }

    /**
     * Retrieves the notifications of this building
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class)->orderBy('code');
    }
}
