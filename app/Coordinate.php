<?php

namespace AlphaDengue;

/**
 * Class Coordinates
 *
 * @package AlphaDengue
 * @author Caique de Castro<caiquecastro@outlook.com>
 */
class Coordinate
{
    /**
     * @var float
     */
    private $latitude;

    /**
     * @var float
     */
    private $longitude;

    /**
     * Coordinates constructor.
     *
     * @param float $latitude  The latitude for the coordinate
     * @param float $longitude The longitude for the coordinate
     */
    public function __construct($latitude, $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    public function latitude()
    {
        return $this->latitude;
    }

    /**
     * Returns the longitude for the coordinate
     *
     * @return float
     */
    public function longitude()
    {
        return $this->longitude;
    }
}