<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Retrieves the notifications with this examination
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
