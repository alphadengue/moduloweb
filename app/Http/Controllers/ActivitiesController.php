<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Activity;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Requests\ActivityRequest;
use Illuminate\Http\Request;

class ActivitiesController extends Controller
{
    /**
     * @var Activity
     */
    protected $activity;

    /**
     * ActivitiesController constructor.
     *
     * @param Activity $activity The activity resource
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activities = $this->activity->paginate();

        return view('activities.index', compact('activities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('activities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ActivityRequest $request The current request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|static
     */
    public function store(ActivityRequest $request)
    {
        $this->activity->create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Atividade criada com sucesso!');

        return redirect('admin/activities');
    }


    /**
     * Display the specified activity.
     *
     * @param Activity $activities The activity resource
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Activity $activities)
    {
        return view('activities.show', compact('activities'));
    }

    /**
     * Show the form for editing the specified activity.
     *
     * @param int $id The resource id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $activity = Activity::findOrFail($id);
        return view('activities.edit', compact('activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ActivityRequest $request The current request
     * @param int             $id      The resource id
     *
     * @return Response
     */
    public function update(ActivityRequest $request, $id)
    {
        $activity = $this->activity->findOrFail($id);
        $activity->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Atividade atualizada com sucesso!');

        return redirect('/admin/activities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request The current request
     * @param int     $id      The activity identifier
     *
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $delete_count = $this->activity->destroy($id);

        if ($delete_count != 1) {
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Erro ao excluir atividade!');
        } else {
            $request->session()->flash('message_type', 'success');
            $request->session()->flash('message', 'Atividade excluida com sucesso!');
        }

        return redirect('/admin/activities');
    }
}
