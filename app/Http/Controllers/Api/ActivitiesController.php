<?php

namespace AlphaDengue\Http\Controllers\Api;

use AlphaDengue\Activity;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\ActivityRequest;

/**
 * Class ActivitiesController
 *
 * @package AlphaDengue\Http\Controllers\Api
 */
class ActivitiesController extends Controller
{
    /** @var Activity The Activity Eloquent instance */
    private $activity;

    /**
     * ActivitiesController constructor.
     *
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->activity->paginate());
    }

    /**
     * @param Activity $activity
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Activity $activity)
    {
        return response()->json([
            'data' => $activity
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\ActivityRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityRequest $request)
    {
        $activity = $this->activity->create($request->all());

        return response()->json($activity, 201);
    }
}