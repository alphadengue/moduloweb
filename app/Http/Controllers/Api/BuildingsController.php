<?php

namespace AlphaDengue\Http\Controllers\Api;

use AlphaDengue\Building;
use Illuminate\Http\Request;
use AlphaDengue\Services\GeocodingService;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\BuildingRequest;

class BuildingsController extends Controller
{
    /** @var Building The Building entity */
    private $building;

    /**
     * BuildingsController constructor.
     *
     * @param Building $building The Building entity
     */
    public function __construct(Building $building)
    {
        $this->building = $building;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->building->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\BuildingRequest $request The current request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BuildingRequest $request, GeocodingService $geocoder)
    {
        $streetId = $request->input('street_id');
        $streetNumber = $request->input('street_number');

        $coordinate = $geocoder->forStreet($streetId, $streetNumber);
        $postData = $request->all();
        $postData['latitude'] = $coordinate->latitude();
        $postData['longitude'] = $coordinate->longitude();

        $building = $this->building->create($postData);

        return response()->json($building, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \AlphaDengue\Building $building The building to show
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Building $building)
    {
        return response()->json([
            'data' => $building
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\BuildingRequest $request The current request
     * @param \AlphaDengue\Building $building The building to update
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Building $building)
    {
        $building->update($request->all());

        return response()->json($building, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \AlphaDengue\Building $building The building to delete
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Building $building)
    {
        $building->delete();

        return response()->json($building, 200);
    }
}
