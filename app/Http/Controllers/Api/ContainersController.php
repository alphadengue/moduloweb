<?php

namespace AlphaDengue\Http\Controllers\Api;

use AlphaDengue\Container;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\ContainerRequest;

class ContainersController extends Controller
{
    /** @var Container The Container Eloquent instance */
    private $container;

    /**
     * ActivitiesController constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->container->paginate());
    }

    /**
     * @param \AlphaDengue\Container
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Container $container)
    {
        return response()->json([
            'data' => $container
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\ContainerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContainerRequest $request)
    {
        $container = $this->container->create($request->all());

        return response()->json($container, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\ContainerRequest $request
     * @param Container $container
     * @return \Illuminate\Http\Response
     */
    public function update(ContainerRequest $request, Container $container)
    {
        $container->update($request);

        return response()->json($container, 200);
    }
}
