<?php

namespace AlphaDengue\Http\Controllers\Api;

use AlphaDengue\State;
use AlphaDengue\Http\Requests\StateRequest;
use AlphaDengue\Http\Controllers\Controller;

/**
 * Class StatesController
 *
 * @package AlphaDengue\Http\Controllers\Api
 */
class StatesController extends Controller
{
    /** @var State The State Eloquent instance */
    private $state;

    /**
     * StatesController constructor.
     *
     * @param State $state The State entity.
     */
    public function __construct(State $state)
    {
        $this->state = $state;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json($this->state->paginate());
    }

    /**
     * @param State $states
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(State $states)
    {
        return response()->json([
            'data' => $states
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\StateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        $state = $this->state->create($request->all());

        return response()->json($state, 201);
    }
}