<?php

namespace AlphaDengue\Http\Controllers\Api;

use DB;
use AlphaDengue\Visit;
use AlphaDengue\Http\Requests\VisitRequest;
use AlphaDengue\Http\Controllers\Controller;

class VisitsController extends Controller
{
    /** @var \AlphaDengue\Visit */
    private $visit;

    /**
     * VisitsController constructor.
     * @param Visit $visit
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VisitRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitRequest $request)
    {
        $user = $request->user();
        $requestData = $request->all();

        $visit = new Visit($requestData);

        if (! $request->has('agent_id')) {
            $visit->agent_id = $user->id;
        }

        DB::transaction(function () use ($visit, $user, $requestData) {
            $user->visits()->save($visit);

            $containersData = $requestData['containers'] ?? [];

            foreach ($containersData as $containerData) {
                $visit->addContainer($containerData);
            }
        });

        return response()->json($visit, 201);
    }

}
