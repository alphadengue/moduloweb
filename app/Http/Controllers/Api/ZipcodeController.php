<?php

namespace AlphaDengue\Http\Controllers\Api;

use Illuminate\Http\Request;
use AlphaDengue\Queries\StreetsQuery;
use AlphaDengue\Http\Controllers\Controller;

class ZipcodeController extends Controller
{
    /**
     * @var StreetsQuery
     */
    private $streetQuery;

    /**
     * ZipcodeController constructor.
     *
     * @param StreetsQuery $streetQuery The street query object.
     */
    public function __construct(StreetsQuery $streetQuery)
    {
        $this->middleware('auth:api');
        $this->streetQuery = $streetQuery;
    }

    /**
     * Returns the data from a specific zipcode.
     *
     * @param string  $zipcode The zipcode to look for.
     * @param Request $request The current request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index($zipcode, Request $request)
    {
        $street = $this->streetQuery->getFor($zipcode);

        return response()->json($street);
    }
}
