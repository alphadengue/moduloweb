<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getGeocoding($address)
    {
        /**
         * FIXME:
         * Remove this method and make it a Job
         */
        $param = ['address' => $address];
        $geocoding = json_decode(\Geocoder::geocode('json', $param), true);

        return $geocoding['results'][0]['geometry']['location'];
    }
}
