<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\BuildingStatus;
use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Requests\BuildingStatusRequest;

class BuildingStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $building_statuses = BuildingStatus::all();

        return view('building-statuses.index', compact('building_statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $status = new BuildingStatus;

        return view('building-statuses.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BuildingStatusRequest $request
     * @return Response
     */
    public function store(BuildingStatusRequest $request)
    {
        $building_status = BuildingStatus::create($request->all());

        if ($request->ajax()) {
            return $building_status;
        }

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Situação de imóvel criada com sucesso!');

        return redirect('admin/building-statuses');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return BuildingStatus::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $status = BuildingStatus::findOrFail($id);
        return view('building-statuses.edit', compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, BuildingStatusRequest $request)
    {
        $status = BuildingStatus::findOrFail($id);
        $status->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Situação de imóvel atualizada com sucesso!');

        return redirect('/admin/building-statuses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = BuildingStatus::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir situação de imóvel!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Situação de imóvel excluida com sucesso!');
        }

        return redirect('/admin/building-statuses');
    }
}
