<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\BuildingType;
use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;

use AlphaDengue\Http\Requests\BuildingTypeRequest;
use Illuminate\Http\Request;

class BuildingTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $building_types = BuildingType::all();

        return view('building-types.index', compact('building_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $building_type = new BuildingType;

        return view('building-types.create', compact('building_type'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BuildingTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BuildingTypeRequest $request)
    {
        BuildingType::create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Tipo de Imóvel criado com sucesso!');

        return redirect('admin/building-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(BuildingType $building_type)
    {
        return response()->json(
            $building_type
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $building_type = BuildingType::findOrFail($id);

        return view('building-types.edit', compact('building_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param BuildingTypeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, BuildingTypeRequest $request)
    {
        $building_type = BuildingType::findOrFail($id);
        $building_type->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Tipo de imóvel atualizado com sucesso!');

        return redirect('/admin/building-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = BuildingType::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir tipo de imóvel!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Tipo de imóvel excluido com sucesso!');
        }

        return redirect('/admin/building-types');
    }
}
