<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use AlphaDengue\District;
use AlphaDengue\Region;
use AlphaDengue\State;
use AlphaDengue\Building;
use AlphaDengue\Street;
use Illuminate\Http\Request;
use AlphaDengue\BuildingType;
use AlphaDengue\Services\GeocodingService;
use AlphaDengue\Http\Requests\BuildingRequest;

class BuildingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buildings = Building::orderBy('street_number')->paginate();

        return view('buildings.index', compact('buildings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $building_types = BuildingType::orderBy('name')->pluck('name', 'id');
        $states = State::orderBy('name')->pluck('name', 'id');
        $cities = $regions = $districts = $streets = [];
        $building = new Building;

        return view(
            'buildings.create',
            compact(
                'building_types',
                'states',
                'cities',
                'regions',
                'districts',
                'streets',
                'building'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BuildingRequest $request The income request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BuildingRequest $request, GeocodingService $geocoder)
    {
        $streetId = $request->input('street_id');
        $streetNumber = $request->input('street_number');

        $coordinate = $geocoder->forStreet($streetId, $streetNumber);

        $buildingRequest = $request->all();
        $buildingRequest['latitude'] = $coordinate->latitude();
        $buildingRequest['longitude'] = $coordinate->longitude();

        $building = Building::create($buildingRequest);

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Imóvel cadastrado com sucesso!');

        return redirect('/admin/buildings');
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request The current request
     * @param int     $id      The resource id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $building = Building::findOrFail($id);

        return view('buildings.show', compact('building'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $building = Building::findOrFail($id);
        $building_types = BuildingType::orderBy('name')->pluck('name', 'id');

        $streets = Street::where('district_id', '=', $building->street->district_id)->pluck('name', 'id');
        $districts = District::where('region_id', '=', $building->street->district->region_id)->pluck('name', 'id');
        $regions = Region::where('city_id', '=', $building->street->district->region->city_id)->pluck('name', 'id');
        $cities = City::where('state_id', '=', $building->street->district->region->city->state_id)->pluck('name', 'id');
        $states = State::orderBy('name')->pluck('name', 'id');

        return view(
            'buildings.edit',
            compact('building', 'building_types', 'states', 'cities', 'regions', 'districts', 'streets')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param BuildingRequest $request
     * @return Response
     */
    public function update($id, BuildingRequest $request, GeocodingService $geocoder)
    {
        $building = Building::findOrFail($id);

        $streetId = $request->input('street_id');
        $streetNumber = $request->input('street_number');

        $coordinate = $geocoder->forStreet($streetId, $streetNumber);

        $buildingRequest = $request->all();

        $buildingRequest['latitude'] = $coordinate->latitude();
        $buildingRequest['longitude'] = $coordinate->longitude();

        $building->update($buildingRequest);

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Imóvel atualizado com sucesso!');

        return redirect('/admin/buildings');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $delete_count = Building::destroy($id);

        if ($delete_count != 1) {
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Erro ao excluir imóvel!');
        } else {
            $request->session()->flash('message_type', 'success');
            $request->session()->flash('message', 'Imóvel excluido com sucesso!');
        }

        return redirect('/admin/buildings');
    }
}
