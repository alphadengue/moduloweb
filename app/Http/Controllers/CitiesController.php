<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use \AlphaDengue\State;
use Illuminate\Http\Request;
use AlphaDengue\Http\Requests\CityRequest;

class CitiesController extends Controller
{
    private $city;

    /**
     * CitiesController constructor.
     * @param City $city
     */
    public function __construct(City $city)
    {
        $this->city = $city;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $citiesQuery = $this->city->orderBy('name');

        if ($request->has('q')) {
            $query = $request->query('q');
            $citiesQuery = $citiesQuery->where('name', 'ilike', '%' . $query . '%');
        }

        $cities = $citiesQuery->with('state')->paginate();

        return view('cities.index', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param State $state The state entity
     *
     * @return \Illuminate\Http\Response
     */
    public function create(State $state)
    {
        $city = new City;
        $states = $state->orderBy('name')->pluck('name', 'id');

        return view('cities.create', compact('city', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CityRequest $request The incoming request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        $this->city->create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Município cadastrado com sucesso!');

        return redirect('/admin/cities');
    }

    /**
     * Display the specified resource.
     *
     * @param City $city
     * @param \Illuminate\Http\Request $request The current request
     *
     * @return \Illuminate\Http\Response
     */
    public function show(City $city, Request $request)
    {
        return view('cities.show', compact('city'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city, State $state)
    {
        $states = $state->orderBy('name')->pluck('name', 'id');

        return view('cities.edit', compact('city', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param CityRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, CityRequest $request)
    {
        $city = City::findOrFail($id);
        $city->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Município atualizado com sucesso!');

        return redirect('/admin/cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_count = City::destroy($id);

        if ($delete_count != 1) {
            session()->flash('message_type', 'danger');
            session()->flash('message', 'Erro ao excluir município!');
        } else {
            session()->flash('message_type', 'success');
            session()->flash('message', 'Município excluido com sucesso!');
        }

        return redirect('/admin/cities');
    }

    /**
     * Get the regions of the city
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function regions($id)
    {
        $city = City::findOrFail($id);

        return response()->json([
            'data' => $city->regions
        ]);
    }

    /**
     * Searches a city by the name criteria
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function search(Request $request)
    {
        $query = $request->input('q');
        $cities = City::where('name', 'ilike', '%' . $query . '%')->orderBy('name');

        if ($request->ajax()) {
            return $cities->get();
        }

        $cities = $cities->paginate();

        return view('cities.index', compact('cities'));
    }
}
