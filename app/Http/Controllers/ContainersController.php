<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Container;
use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;

use AlphaDengue\Http\Requests\ContainerRequest;
use Illuminate\Http\Request;

class ContainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $containers = Container::paginate();

        return view('containers.index', compact('containers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('containers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContainerRequest $request
     * @return Response
     */
    public function store(ContainerRequest $request)
    {
        $container = Container::create($request->all());

        if ($request->ajax()) return $container;

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Recipiente criado com sucesso!');

        return redirect('admin/containers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Container::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $container = Container::findOrFail($id);
        return view('containers.edit', compact('container'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ContainerRequest $request
     * @return Response
     */
    public function update($id, ContainerRequest $request)
    {
        $container = Container::findOrFail($id);
        $container->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Recipiente atualizado com sucesso!');

        return redirect('/admin/containers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = Container::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir recipiente!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Recipiente excluido com sucesso!');
        }

        return redirect('/admin/containers');
    }
}
