<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use AlphaDengue\Region;
use AlphaDengue\State;
use AlphaDengue\District;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use AlphaDengue\Http\Requests\DistrictRequest;

class DistrictsController extends Controller
{
    /**
     * @var District
     */
    private $district;

    public function __construct(District $district)
    {
        $this->district = $district;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $districtsQuery = $this->district->orderBy('name');

        if ($request->has('q')) {
            $query = $request->query('q');
            $districtsQuery = $districtsQuery->where('name', 'ilike', '%' . $query . '%');
        }

        $districts = $districtsQuery->with('region.city')->paginate();

        return view('districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $district = new District;
        $regions = [];
        $cities = [];
        $states = State::orderBy('name')->pluck('name', 'id');

        return view('districts.create', compact('states', 'cities', 'regions', 'district'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DistrictRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DistrictRequest $request)
    {
        District::create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Bairro cadastrado com sucesso!');

        return redirect('/admin/districts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $district = District::findOrFail($id);

        return view('districts.show', compact('district'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $district = District::findOrFail($id);
        $states = State::orderBy('name')->pluck('name', 'id');
        $cities = City::where('state_id', '=', $district->region->city->state_id)->pluck('name', 'id');
        $regions = Region::where('city_id', '=', $district->region->city_id)->pluck('name', 'id');

        return view('districts.edit', compact('district', 'states', 'cities', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param DistrictRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, DistrictRequest $request)
    {
        $district = District::findOrFail($id);
        $district->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Bairro atualizado com sucesso!');

        return redirect('/admin/districts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int     $id      The id of the resource to be deleted.
     * @param Request $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        try {
            $district = District::findOrFail($id);

            $district->delete();

            $request->session()->flash('message_type', 'success');
            $request->session()->flash('message', 'Bairro excluido com sucesso');
        } catch (QueryException $e) {
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Erro ao excluir bairro');
        }

        return redirect('/admin/districts');
    }

    /**
     * Get the streets of the district
     *
     * @param $id
     * @return mixed
     */
    public function streets($id)
    {
        $district = District::findOrFail($id);

        return response()->json([
            'data' => $district->streets
        ]);
    }
}
