<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Examination;
use AlphaDengue\Http\Requests\ExaminationRequest;

class ExaminationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $examinations = Examination::paginate();

        return view('examinations.index', compact('examinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $examination = new Examination;

        return view('examinations.create', compact('examination'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ExaminationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExaminationRequest $request)
    {
        Examination::create($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Exame criado com sucesso!');

        return redirect('admin/examinations');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Examination::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $examination = Examination::findOrFail($id);

        return view('examinations.edit', compact('examination'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ExaminationRequest $request
     * @return Response
     */
    public function update($id, ExaminationRequest $request)
    {
        $examination = Examination::findOrFail($id);
        $examination->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Exame atualizado com sucesso!');

        return redirect('/admin/examinations');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = Examination::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir exame!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Exame excluido com sucesso!');
        }

        return redirect('/admin/examinations');
    }

}
