<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\LarvaRequest;
use AlphaDengue\Larva;
use Illuminate\Http\Request;

class LarvaeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $larvae = Larva::all();

        return view('larvae.index', compact('larvae'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('larvae.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LarvaRequest $request
     * @return Response
     */
    public function store(LarvaRequest $request)
    {
        $larva = Larva::create($request->all());

        if ($request->ajax()) {
            return $larva;
        }

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Larva criada com sucesso!');

        return redirect('admin/larvae');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Larva::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $larva = Larva::findOrFail($id);
        return view('larvae.edit', compact('larva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param LarvaRequest $request
     * @return Response
     */
    public function update($id, LarvaRequest $request)
    {
        $larva = Larva::findOrFail($id);
        $larva->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Larva atualizada com sucesso!');

        return redirect('/admin/larvae');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = Larva::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir larva!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Larva excluida com sucesso!');
        }

        return redirect('/admin/larvae');
    }
}
