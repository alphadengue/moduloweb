<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use AlphaDengue\State;
use AlphaDengue\Examination;
use AlphaDengue\Notification;
use AlphaDengue\Http\Requests\NotificationRequest;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::orderBy('code')->paginate();

        return view('notifications.index', compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::orderBy('abbreviation')->pluck('abbreviation', 'id');
        $all_cities = City::orderBy('name')->pluck('name', 'id');
        $examinations = Examination::pluck('name', 'id');
        $notification = new Notification();

        return view(
            'notifications.create',
            compact('states', 'all_cities', 'examinations', 'notification', 'building_streets')
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NotificationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(NotificationRequest $request)
    {
        $request->user()->notifications()->create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Notificação criada com sucesso!');

        return redirect('/admin/notifications');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Notification::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = Notification::findOrFail($id);

        $states = State::pluck('abbreviation', 'id');
        $all_cities = City::pluck('name', 'id');
        $examinations = Examination::pluck('name', 'id');

        return view(
            'notifications.edit',
            compact('notification', 'all_cities', 'states', 'examinations')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param NotificationRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, NotificationRequest $request)
    {
        $notification = Notification::findOrFail($id);
        $notification->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Notificação atualizada com sucesso!');

        return redirect('/admin/notifications');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = Notification::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir notificação!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Notificação excluida com sucesso!');
        }

        return redirect('/admin/notifications');
    }

    public function positives(\Illuminate\Http\Request $request)
    {
        $dateFrom = $request->query('date-from');
        $dateTo = $request->query('date-to');

        $query = \DB::table('notifications')
            ->join('buildings', 'buildings.id', '=', 'notifications.building_id')
            ->select('latitude', 'longitude')
            ->where('result', '=', 'positive');

        if ($dateFrom) {
            $query->where('notification_date', '>=', $dateFrom);
        }

        if ($dateTo) {
            $query->where('notification_date', '<=', $dateTo);
        }

        return $query->get();
    }

}
