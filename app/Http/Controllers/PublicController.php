<?php

namespace AlphaDengue\Http\Controllers;

use Illuminate\Mail\Mailer;
use AlphaDengue\Http\Requests\ContactFormRequest;

/**
 * Class PublicController
 *
 * @package AlphaDengue\Http\Controllers
 * @author  Caique de Castro <caiquecastro@outlook.com>
 */
class PublicController extends Controller
{
    /**
     * Displays the public index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('public.index');
    }

    /**
     * Displays the public about page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function vigilancia()
    {
        return view('public.vigilancia');
    }

    /**
     * Displays the public page about the mosquito
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dengue()
    {
        return view('public.dengue');
    }

    /**
     * Display the contact form page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getContato()
    {
        return view('public.contato');
    }

    /**
     * Sends the contact message from the form
     *
     * @param ContactFormRequest $request The validation request
     * @param Mailer             $mailer  The mail
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postContato(ContactFormRequest $request, Mailer $mailer)
    {
        $mailer->queue(
            'emails.contato',
            [
                'name' => $request->get('nome'),
                'email' => $request->get('email'),
                'subject' => $request->get('assunto'),
                'content' => $request->get('mensagem')
            ],
            function ($message) {
                $message->subject('Email de contato')
                    ->from('site@alphadengue.com.br')
                    ->to('castro.caique@gmail.com', 'Contato');
            }
        );

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Obrigado por entrar em contato');

        return back();
    }
}
