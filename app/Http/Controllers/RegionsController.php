<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use AlphaDengue\State;
use AlphaDengue\Region;
use Illuminate\Http\Request;
use AlphaDengue\Http\Requests\RegionRequest;

class RegionsController extends Controller
{
    private $region;

    /**
     * RegionsController constructor.
     * @param Region $region
     */
    public function __construct(Region $region)
    {
        $this->region = $region;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $regionsQuery = $this->region->orderBy('name');

        if ($request->has('q')) {
            $query = $request->query('q');
            $regionsQuery = $regionsQuery->where('name', 'ilike', '%' . $query . '%');
        }

        $regions = $regionsQuery->with('city.state')->paginate();

        return view('regions.index', compact('regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $region = new Region;
        $states = State::orderBy('name')->pluck('name', 'id');
        $cities = [];

        return view('regions.create', compact('region', 'states', 'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RegionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegionRequest $request)
    {
        Region::create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Região cadastrada com sucesso!');

        return redirect('/admin/regions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $region = Region::findOrFail($id);

        return view('regions.show', compact('region'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $region = Region::findOrFail($id);
        $states = State::orderBy('name')->pluck('name', 'id')->all();
        $cities = City::where('state_id', '=', $region->city->state_id)->pluck('name', 'id');

        return view('regions.edit', compact('region', 'cities', 'states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param RegionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, RegionRequest $request)
    {
        $region = Region::findOrFail($id);
        $region->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Região atualizada com sucesso!');

        return redirect('/admin/regions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_count = Region::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir região!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Região excluida com sucesso!');
        }

        return redirect('/admin/regions');
    }

    /**
     * Get the districts of the region
     *
     * @param $id
     * @return mixed
     */
    public function districts($id)
    {
        $region = Region::findOrFail($id);

        return response()->json([
            'data' => $region->districts
        ]);
    }
}
