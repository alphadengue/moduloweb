<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Repositories\NotificationRepository;

class ReportsController extends Controller
{
    private $notificationsRepository;

    /**
     * ReportsController constructor.
     *
     * @param NotificationRepository $notificationsRepository
     */
    public function __construct(NotificationRepository $notificationsRepository)
    {
        $this->notificationsRepository = $notificationsRepository;
    }

    /**
     * Displays the reports index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('reports.index', compact('notificationsByStatus'));
    }

    /**
     * Returns the notifications grouped by status
     *
     * @return \Illuminate\Http\Response
     */
    public function notificationsByStatus()
    {
        return $this->notificationsRepository->getByStatus();
    }

    public function notificationsByAge()
    {
        return $this->notificationsRepository->groupByAge();
    }

    public function notificationsByRegion()
    {
        return $this->notificationsRepository->groupByRegion();
    }
}
