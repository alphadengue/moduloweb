<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Role;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use AlphaDengue\Http\Requests\RoleRequest;
use AlphaDengue\Repositories\RolesRepository;

class RolesController extends Controller
{
    private $rolesRepository;

    public function __construct(RolesRepository $rolesRepository)
    {
        $this->rolesRepository = $rolesRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::withCount('users')->paginate();

        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = new Role;

        return view('roles.create', compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request The current request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $this->rolesRepository->create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Cargo criado com sucesso!');

        return redirect('admin/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id The role id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Role $role
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest|Request $request The current request
     * @param Role $role
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $role->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Cargo atualizado com sucesso!');

        return redirect('admin/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Role $role
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role, Request $request)
    {
        if ($role->users()->count()) {

        }

        $role->delete();

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Cargo excluido com sucesso');

        try {

        } catch (QueryException $e) {
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Erro ao excluir o cargo');
        }

        return redirect('/admin/roles');
    }
}
