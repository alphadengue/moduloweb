<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Settings;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display the main page of settings pages
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Settings::all();

        return view('settings.index', compact('settings'));
    }
}
