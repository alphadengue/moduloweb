<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\State;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use AlphaDengue\Http\Requests\StateRequest;

/**
 * Class StatesController
 *
 * @package AlphaDengue\Http\Controllers
 * @author  Caique de Castro <caiquecastro@outlook.com>
 */
class StatesController extends Controller
{
    /**
     * @var State
     */
    private $state;

    /**
     * StatesController constructor.
     * @param State $state
     */
    public function __construct(State $state)
    {
        $this->state = $state;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $statesQuery = $this->state->orderBy('name');

        if ($request->has('q')) {
            $query = $request->query('q');
            $statesQuery = $statesQuery->where('name', 'ilike', '%' . $query . '%');
        }

        $states = $statesQuery->paginate();

        return view('states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $state = new State;

        return view('states.create', compact('state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \AlphaDengue\Http\Requests\StateRequest $request The request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        $this->state->create($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Estado cadastrado com sucesso!');

        return redirect('/admin/states');
    }

    /**
     * Display the specified resource.
     *
     * @param \AlphaDengue\State $state The state to show.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {
        return view('states.show', compact('state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = $this->state->findOrFail($id);

        return view('states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param StateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, StateRequest $request)
    {
        $state = $this->state->findOrFail($id);
        $state->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Estado atualizado com sucesso!');

        return redirect('/admin/states');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int     $id      The id of the resource to be deleted.
     * @param Request $request The income request.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state, Request $request)
    {
        try {
            $state->delete();

            $request->session()->flash('message_type', 'success');
            $request->session()->flash('message', 'Estado excluido com sucesso');
        } catch (QueryException $e) {
            $request->session()->flash('message_type', 'danger');
            $request->session()->flash('message', 'Erro ao excluir o estado');
        }

        return redirect('/admin/states');
    }

    /**
     * Get the cities of the state
     *
     * @param int $id The id of the resource to get the cities.
     *
     * @return \Illuminate\Http\Response
     */
    public function cities($id)
    {
        $state = State::findOrFail($id);

        return response()->json([
            'data' => $state->cities
        ]);
    }
}
