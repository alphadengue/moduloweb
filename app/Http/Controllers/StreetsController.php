<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\City;
use AlphaDengue\District;
use AlphaDengue\Region;
use AlphaDengue\State;
use AlphaDengue\Street;
use Illuminate\Http\Request;
use AlphaDengue\Http\Requests\StreetRequest;

class StreetsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $streetsQuery = Street::orderBy('name');

        if ($request->has('q')) {
            $query = $request->query('q');
            $streetsQuery = $streetsQuery->where('name', 'ilike', '%' . $query . '%');
        }

        $streets = $streetsQuery->paginate();

        return view('streets.index', compact('streets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $states = State::orderBy('name')->pluck('name', 'id');
        $cities = [];
        $regions = [];
        $districts = [];
        $street = new Street;

        return view('streets.create', compact('states', 'cities', 'regions', 'districts', 'street'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StreetRequest $request
     * @return Response
     */
    public function store(StreetRequest $request)
    {
        Street::create($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Logradouro cadastrado com sucesso!');

        return redirect('/admin/streets');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param Request $request
     * @return Response
     */
    public function show($id, Request $request)
    {
        $street = Street::findOrFail($id);

        return view('streets.show', compact('street'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $street = Street::findOrFail($id);
        $districts = District::where('region_id', '=', $street->district->region_id)->pluck('name', 'id');
        $regions = Region::where('city_id', '=', $street->district->region->city_id)->pluck('name', 'id');
        $cities = City::where('state_id', '=', $street->district->region->city->state_id)->pluck('name', 'id');
        $states = State::orderBy('name')->pluck('name', 'id');
        return view('streets.edit', compact('street', 'states', 'cities', 'regions', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param StreetRequest $request
     * @return Response
     */
    public function update($id, StreetRequest $request)
    {
        $street = Street::findOrFail($id);
        $street->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Logradouro atualizado com sucesso!');

        return redirect('/admin/streets');
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function destroy($id)
    {
        $delete_count = Street::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir logradouro!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Logradouro excluido com sucesso!');
        }

        return redirect('/admin/streets');
    }

    /**
     * Get the buildings of the street
     *
     * @param $id
     * @return mixed
     */
    public function getBuildings($id)
    {
        $street = Street::findOrFail($id);

        $buildings = $street->buildings()->with(['building_type'])->get();

        return response()->json([
            'data' => $buildings
        ]);
    }
}
