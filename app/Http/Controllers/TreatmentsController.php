<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\TreatmentRequest;
use AlphaDengue\Treatment;
use Illuminate\Http\Request;

class TreatmentsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $treatments = Treatment::all();

        return view('treatments.index', compact('treatments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('treatments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TreatmentRequest $request
     * @return Response
     */
    public function store(TreatmentRequest $request)
    {
        $treatment = Treatment::create($request->all());

        if ($request->ajax()) return $treatment;

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Tratamento criado com sucesso!');

        return redirect('admin/treatments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return Treatment::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $treatment = Treatment::find($id);
        return view('treatments.edit', compact('treatment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param TreatmentRequest $request
     * @return Response
     */
    public function update($id, TreatmentRequest $request)
    {
        $treatment = Treatment::findOrFail($id);
        $treatment->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Tratamento atualizado com sucesso!');

        return redirect('/admin/treatments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $delete_count = Treatment::destroy($id);

        if ($delete_count != 1) {
            \Session::flash('message_type', 'danger');
            \Session::flash('message', 'Erro ao excluir tratamento!');
        } else {
            \Session::flash('message_type', 'success');
            \Session::flash('message', 'Tratamento excluido com sucesso!');
        }

        return redirect('/admin/treatments');
    }

}
