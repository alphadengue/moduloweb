<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Role;
use AlphaDengue\User;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use AlphaDengue\Http\Requests\UserRequest;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->get();

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $roles = Role::pluck('name', 'id');

        return view('users.create', compact('user', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user = new User($request->all());
        $user->api_token = str_random(60);

        $user->save();

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Usuário criado com sucesso!');

        return redirect('admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return response()->json(
            $user
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name', 'id');

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user, UserRequest $request)
    {
        $user->update($request->all());

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Usuário criado com sucesso!');

        return redirect()->route('users.show', [$user]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();

            session()->flash('message_type', 'success');
            session()->flash('message', 'Usuário excluido com sucesso!');
        } catch (QueryException $e) {
            session()->flash('message_type', 'danger');
            session()->flash('message', 'Erro ao excluir usuário!');
        }

        return redirect('/admin/users');
    }
}
