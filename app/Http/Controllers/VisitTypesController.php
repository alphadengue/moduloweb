<?php

namespace AlphaDengue\Http\Controllers;

use AlphaDengue\Http\Requests;
use AlphaDengue\Http\Controllers\Controller;
use AlphaDengue\Http\Requests\VisitTypeRequest;
use AlphaDengue\VisitType;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class VisitTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $visit_types = VisitType::all();

        return view('visit-types.index', compact('visit_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('visit-types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VisitTypeRequest $request
     * @return Response
     */
    public function store(VisitTypeRequest $request)
    {
        $visit_type = VisitType::create($request->all());

        if ($request->ajax()) {
            return $visit_type;
        }

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Tipo de visita criado com sucesso!');

        return redirect('admin/visit-types');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return VisitType::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $visit_type = VisitType::findOrFail($id);
        return view('visit-types.edit', compact('visit_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param VisitTypeRequest $request
     * @return Response
     */
    public function update($id, VisitTypeRequest $request)
    {
        $visit_type = VisitType::findOrFail($id);
        $visit_type->update($request->all());

        \Session::flash('message_type', 'success');
        \Session::flash('message', 'Tipo de visita atualizada com sucesso!');

        return redirect('/admin/visit-types');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VisitType $visit_type)
    {
        try {
            $visit_type->delete();

            session()->flash('message_type', 'success');
            session()->flash('message', 'Tipo de visita excluida com sucesso!');
        } catch (QueryException $e) {
            session()->flash('message_type', 'danger');
            session()->flash('message', 'Erro ao excluir tipo de visita!');
        }

        return redirect('/admin/visit-types');
    }
}
