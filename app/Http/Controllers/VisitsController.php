<?php

namespace AlphaDengue\Http\Controllers;

use Auth;
use Geocoder;
use AlphaDengue\User;
use AlphaDengue\State;
use AlphaDengue\Visit;
use AlphaDengue\Activity;
use AlphaDengue\Container;
use AlphaDengue\Treatment;
use AlphaDengue\VisitType;
use AlphaDengue\BuildingStatus;
use AlphaDengue\Http\Requests\VisitRequest;
use Illuminate\Http\Request as IlluminateRequest;

class VisitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $visits = Visit::with('building', 'activity', 'visit_type')->get();

        return view('visits.index', compact('visits'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'visit' => new Visit,
            'activities' => Activity::pluck('name', 'id')->all(),
            'visit_types' => VisitType::pluck('name', 'id')->all(),
            'agents' => User::pluck('name', 'id'),
            'states' => State::pluck('abbreviation', 'id')->all(),
            'building_statuses' => BuildingStatus::pluck('name', 'id')->all(),
            'containers' => Container::pluck('name', 'id')->all(),
            'treatments' => Treatment::pluck('name', 'id')->all()
        ];

        return view('visits.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VisitRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitRequest $request)
    {
        \DB::transaction(function () use ($request) {
            $visit = new Visit($request->all());

            $request->user()->visits()->save($visit);

            $containersId = $request->container_id;
            $amounts = $request->amount;
            $waterAmounts = $request->water_amount;
            $larvaAmounts = $request->larva_amount;
            $treatments = $request->treatment_id;
            $samples = $request->sample;

            for ($i = 0; $i < count($containersId); $i++) {
                $visit->addContainer([
                    'container_id' => $containersId[$i],
                    'total_amount' => $amounts[$i],
                    'water_amount' => $waterAmounts[$i],
                    'larva_amount' => $larvaAmounts[$i],
                    'treatment_id' => $treatments[$i],
                    'sample' => $samples[$i]
                ]);
            }
        });

        $request->session()->flash('message_type', 'success');
        $request->session()->flash('message', 'Visita cadastrada com sucesso!');

        return redirect('/admin/visits');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $visit = Visit::with('containers')->find($id);

        return view('visits.show', compact('visit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
            'visit' => Visit::findOrFail($id),
            'activities' => Activity::pluck('name', 'id'),
            'visit_types' => VisitType::pluck('name', 'id'),
            'agents' => User::pluck('name', 'id'),
            'states' => State::pluck('abbreviation', 'id'),
            'building_statuses' => BuildingStatus::pluck('name', 'id'),
            'containers' => Container::pluck('name', 'id'),
            'treatments' => Treatment::pluck('name', 'id')
        ];

        return view('visits.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getPoints(IlluminateRequest $request)
    {
        $dateFrom = $request->query('date-from');
        $dateTo = $request->query('date-to');

        $query = \DB::table('visits')
            ->join('buildings', 'buildings.id', '=', 'visits.building_id')
            ->select('latitude', 'longitude');

        if ($dateFrom) {
            $query->where('visited_at', '>=', $dateFrom);
        }

        if ($dateTo) {
            $query->where('visited_at', '<=', $dateTo);
        }

        return $query->get();
    }
}
