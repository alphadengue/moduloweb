<?php

namespace AlphaDengue\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (!$request->ajax()) {
                return redirect('/admin');
            } else {
                // TODO: adjust correct response code
                return response(
                    [
                        'status' => 'failed',
                        'errors' => ['Usuário já conectado. Redirecionando...'],
                        'redirectPath' => '/admin'
                    ],
                    200
                );
            }
        }

        return $next($request);
    }

}
