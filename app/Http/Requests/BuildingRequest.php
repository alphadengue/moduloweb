<?php

namespace AlphaDengue\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BuildingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'street_id' => 'required|exists:streets,id',
            'street_number' => 'required',
            'building_type_id' => 'required|exists:building_types,id'
        ];
    }
}
