<?php

namespace AlphaDengue\Http\Requests;

class NotificationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|numeric',
            'notification_date' => 'required|date',
            'first_signs_date' => 'required|date',
            'patient_name' => 'required',
            'birth_date' => 'required|date',
            'patient_mother' => 'required',
            'patient_phone' => 'required',
            'building_id' => 'required',
            'examination_id' => 'required',
            'notification_city_id' => 'required',
            'contamination_city_id' => 'required',
            'closing_date' => 'date'
        ];
    }
}
