<?php

namespace AlphaDengue\Http\Requests;

class VisitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'building_id' => 'required|exists:buildings,id',
            'building_status_id' => 'required|exists:building_statuses,id',
            'activity_id' => 'required|exists:activities,id',
            'visit_type_id' => 'required|exists:visit_types,id',
            'visited_at' => 'required',
        ];
    }
}
