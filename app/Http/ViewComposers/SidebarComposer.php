<?php

namespace AlphaDengue\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Route;

class SidebarComposer
{
    private $currentPage;

    /**
     * SidebarComposer constructor.
     */
    public function __construct()
    {
        $this->currentPage = Route::currentRouteName();
    }

    /**
     * Compose the view specific variables.
     *
     * @param View $view The view to be modified.
     *
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('currentPage', $this->currentPage);

        $view->with('visitsMenuEnabled', $this->hasVisitMenuEnabled());
        $view->with('notificationsMenuEnabled', $this->hasNotificationMenuEnabled());
        $view->with('locationsMenuEnabled', $this->hasLocationMenuEnabled());
        $view->with('aclMenuEnabled', $this->hasAclMenuEnabled());
    }

    /**
     * Indicates whether the visits menu should be active or not.
     *
     * @return bool
     */
    private function hasVisitMenuEnabled()
    {
        return in_array($this->currentPage, [
            'visits.index',
            'visits.create',
            'visits.edit',
            'visit-types.index',
            'visit-types.create',
            'visit-types.edit',
            'activities.index',
            'activities.create',
            'activities.edit',
            'larvae.index',
            'larvae.create',
            'larvae.edit',
            'containers.index',
            'containers.create',
            'containers.edit',
            'treatments.index',
            'treatments.create',
            'treatments.edit',
            'building-statuses.index',
            'building-statuses.create',
            'building-statuses.edit',
        ]);
    }

    private function hasNotificationMenuEnabled()
    {
        $notificationMenuItems = [
            'notifications.index',
            'notifications.create',
            'notifications.edit',
            'examinations.index',
            'examinations.create',
            'examinations.edit',
        ];

        return in_array($this->currentPage, $notificationMenuItems);
    }

    private function hasLocationMenuEnabled()
    {
        return in_array($this->currentPage, [
            'states.index', 'states.create',
            'states.edit', 'states.show',
            'cities.index', 'cities.create',
            'cities.edit', 'cities.show',
            'regions.index', 'regions.create',
            'regions.edit', 'regions.show',
            'districts.index', 'districts.create',
            'districts.edit', 'districts.show',
            'streets.index', 'streets.create',
            'streets.edit', 'streets.show',
            'buildings.index',
            'building-types.index', 'building-types.create', 'building-types.edit',
        ]);

    }

    private function hasAclMenuEnabled()
    {
        return in_array($this->currentPage, [
            'users.index',
            'users.create',
            'roles.index',
            'roles.create',
        ]);
    }
}
