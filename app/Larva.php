<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Larva extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'larvae';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
