<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'notification_date',
        'first_signs_date',
        'patient_name',
        'birth_date',
        'patient_mother',
        'patient_phone',
        'building_id',
        'closing_date',
        'notification_city_id',
        'contamination_city_id',
        'examination_id',
        'result',
        'observation'
    ];

    /**
     *
     * @param $value
     */
    public function setClosingDateAttribute($value)
    {
        if (empty($value)) {
            $this->attributes['closing_date'] = null;
        }
    }

    /**
     * Retrieves the notification city of this notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notification_city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Retrieves the infection city of this notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function infection_city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Retrieves the building of this notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    /**
     * Retrieves the examination of this notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function examination()
    {
        return $this->belongsTo(Examination::class);
    }
}
