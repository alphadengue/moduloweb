<?php

namespace AlphaDengue\Queries;

use AlphaDengue\Street;

class StreetsQuery
{
    /**
     * Returns the street for a specific zipcode.
     *
     * @param string $zipCode The zipcode to look for.
     *
     * @return \AlphaDengue\Street
     */
    public function getFor($zipCode)
    {
        return Street::where('postcode', '=', $zipCode)
            ->with('buildings')
            ->join('districts', 'streets.district_id', '=', 'districts.id')
            ->join('regions', 'districts.region_id', '=', 'regions.id')
            ->join('cities', 'regions.city_id', '=', 'cities.id')
            ->join('states', 'cities.state_id', '=', 'states.id')
            ->selectRaw('streets.id')
            ->selectRaw('postcode as zipcode')
            ->selectRaw('streets.name as street')
            ->selectRaw('streets.id as street_id')
            ->selectRaw('districts.name as district')
            ->selectRaw('cities.name as city')
            ->selectRaw('states.name as state')
            ->selectRaw('abbreviation as uf')
            ->first();
    }
}