<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'city_id'
    ];

    /**
     * Retrieves the districts of this region
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function districts()
    {
        return $this->hasMany(District::class)->orderBy('name');
    }

    /**
     * Returns the city of this region
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * Returns the state for this region based on its city.
     *
     * @return State
     */
    public function state()
    {
        return $this->city->state;
    }
}
