<?php

namespace AlphaDengue\Repositories;

use DB;

class NotificationRepository
{
    /**
     * @return mixed
     */
    public function getByStatus()
    {
        return DB::table('notifications')
            ->select('result', DB::raw('count(*) as total'))
            ->groupBy('result', 'notification_city_id')
            ->orderBy('result', 'asc')
            ->pluck('total', 'result');
    }

    /**
     * @return mixed
     */
    public function groupByAge()
    {
        return DB::table('notifications')
            ->select(
                'result',
                DB::raw('date_part(\'year\', age(birth_date)) as age'),
                DB::raw('count(*) as total')
            )
            ->groupBy('result', 'age')
            ->orderBy('age')
            ->orderBy('result', 'desc')
            ->get();
    }

    /**
     * @return mixed
     */
    public function groupByRegion()
    {
        return DB::table('notifications')
            ->select(
                'result',
                'regions.name',
                DB::raw('count(*) as total')
            )
            ->join('buildings', 'notifications.building_id', '=', 'buildings.id')
            ->join('streets', 'buildings.street_id', '=', 'streets.id')
            ->join('districts', 'streets.district_id', '=', 'districts.id')
            ->join('regions', 'districts.region_id', '=', 'regions.id')
            ->groupBy('result', 'regions.id')
            ->orderBy('regions.name')
            ->orderBy('result', 'desc')
            ->get();
    }
}