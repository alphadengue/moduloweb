<?php

namespace AlphaDengue\Repositories;

use AlphaDengue\Role;

class RolesRepository
{
    public function create($attributes)
    {
        return Role::create($attributes);
    }

    public function delete(Role $role)
    {
        return $role->delete();
    }
}