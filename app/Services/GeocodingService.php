<?php

namespace AlphaDengue\Services;

use Log;
use Geocoder;
use AlphaDengue\Street;
use AlphaDengue\Coordinate;

/**
 * Class GeocodingService
 * @package AlphaDengue\Services
 */
class GeocodingService
{
    private $logger;

    public function __construct()
    {
        // \Illuminate\Contracts\Logging\Log $logger
        //$this->logger = $logger;
    }

    /**
     * Returns the coordinates for the specified address
     *
     * @param int $streetId        The street id of the building
     * @param string $streetNumber The street number of the building
     *
     * @return Coordinate
     */
    public function forStreet($streetId, $streetNumber)
    {
        $street = Street::findOrFail($streetId);
        $address = $street->fullStreetNameWithNumber($streetNumber);

        $geocoding = Geocoder::getCoordinatesForQuery($address);

        Log::emergency('Geocode for address', [
            'address' => $address,
            'geocoding' => $geocoding
        ]);

        return new Coordinate($geocoding['lat'], $geocoding['lng']);
    }
}