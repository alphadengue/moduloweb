<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = [
        'option', 'value'
    ];
}
