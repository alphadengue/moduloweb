<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'postcode',
        'district_id'
    ];

    /**
     * Get the full address for a street with the specified number
     *
     * @param string $number The number of the building to get the full street name
     *
     * @return string
     */
    public function fullStreetNameWithNumber($number)
    {
        $streetName = $this->name;
        $districtName = $this->district->name;
        $cityName = $this->region()->city->name;

        return sprintf(
            '%s, %s, %s, %s', $streetName, $number, $districtName, $cityName
        );
    }

    /**
     * Returns the district of the street.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * Returns the region of the street.
     *
     * @return mixed
     */
    public function region()
    {
        return $this->district->region;
    }

    public function city()
    {
        return $this->region()->city;
    }

    /**
     * Retrieves the buildings of this street
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buildings()
    {
        return $this->hasMany(Building::class)->orderBy('street_number');
    }
}
