<?php

namespace AlphaDengue;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
