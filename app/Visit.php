<?php

namespace AlphaDengue;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'visit_type_id',
        'activity_id',
        'visited_at',
        'agent_id',
        'building_id',
        'building_status_id',
    ];

    /**
     * The attributes that are of date type
     *
     * @var array
     */
    protected $dates = ['visited_at'];

    /**
     * Returns a new instance of Carbon if visited_at attribute is null.
     *
     * @param \Carbon\Carbon|null $value The visited_at raw value.
     *
     * @return \Carbon\Carbon
     */
    public function getVisitedAtAttribute($value)
    {
        if (null === $value) {
            return new Carbon();
        }

        return new Carbon($value);
    }

    /**
     * Retrieves the building of this visit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function building()
    {
        return $this->belongsTo(Building::class);
    }

    /**
     * Retrieves the type of this visit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function visit_type()
    {
        return $this->belongsTo(VisitType::class);
    }

    /**
     * Retrieves the activity of this visit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function building_status()
    {
        return $this->belongsTo(BuildingStatus::class);
    }

    /**
     * Retrieves the user who inserted the visit into the system
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieves the agent who executed the visit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agent()
    {
        return $this->belongsTo(User::class, 'agent_id');
    }

    /**
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function containers()
    {
        return $this->belongsToMany(Container::class)
            ->withPivot([
                'treatment_id',
                'sample',
                'water_amount',
                'larva_amount',
                'total_amount',
            ]);
    }

    public function addContainer($visitContainer)
    {
        $this->containers()->attach(
            $visitContainer['container_id'],
            [
                'treatment_id' => $visitContainer['treatment_id'],
                'sample' => $visitContainer['sample'],
                'total_amount' => $visitContainer['total_amount'],
                'water_amount' => $visitContainer['water_amount'],
                'larva_amount' => $visitContainer['larva_amount']
            ]
        );
    }
}
