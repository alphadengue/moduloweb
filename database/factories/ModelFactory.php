<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\AlphaDengue\Activity::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});

$factory->define(\AlphaDengue\Building::class, function (Faker\Generator $faker) {
    return [
        'street_id' => function () {
            return factory(\AlphaDengue\Street::class)->create()->id;
        },
        'street_number' => $faker->numberBetween(0, 999),
        'additional_address_details' => null,
        'building_type_id' => function () {
            return factory(\AlphaDengue\BuildingType::class)->create()->id;
        },
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});

$factory->define(\AlphaDengue\BuildingType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\AlphaDengue\City::class, function (Faker\Generator $faker) {
    return [
        'state_id' => function () {
            return factory(\AlphaDengue\State::class)->create()->id;
        },
        'name' => $faker->city
    ];
});

$factory->define(\AlphaDengue\District::class, function (Faker\Generator $faker) {
    return [
        'region_id' => function () {
            return factory(\AlphaDengue\Region::class)->create()->id;
        },
        'district_type' => $faker->randomElement([ 'urban', 'rural' ]),
        'name' => ucfirst($faker->word)
    ];
});

$factory->define(\AlphaDengue\Examination::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\AlphaDengue\Region::class, function (Faker\Generator $faker) {
    return [
        'city_id' => function () {
            return factory(\AlphaDengue\City::class)->create()->id;
        },
        'name' => 'Região ' . $faker->numerify()
    ];
});

$factory->define(AlphaDengue\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->define(\AlphaDengue\State::class, function (Faker\Generator $faker) {
   return [
       'name' => $faker->state,
       'abbreviation' => $faker->stateAbbr
   ];
});

$factory->define(\AlphaDengue\Street::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'postcode' => $faker->numerify('########'),
        'district_id' => function () {
            return factory(AlphaDengue\District::class)->create()->id;
        },
    ];
});

$factory->define(AlphaDengue\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'api_token' => str_random(60),
        'role_id' => function () {
            return factory(AlphaDengue\Role::class)->create()->id;
        },
        'remember_token' => str_random(10),
    ];
});
