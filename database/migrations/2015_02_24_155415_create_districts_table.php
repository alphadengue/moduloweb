<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('region_id')->unsigned();
            //$table->integer('district_type_id')->unsigned();
            $table->enum('district_type', ['urban', 'rural']);
            $table->timestamps();

            $table->foreign('region_id')
                ->references('id')->on('regions');
//            $table->foreign('district_type_id')
//                  ->references('id')->on('district_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
