<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id')->unsigned();
            $table->integer('building_status_id')->unsigned();
            $table->integer('activity_id')->unsigned();
            $table->integer('visit_type_id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('visited_at');
            $table->timestamps();

            $table->foreign('building_id')
                ->references('id')->on('buildings');
            $table->foreign('building_status_id')
                ->references('id')->on('building_statuses');
            $table->foreign('activity_id')
                ->references('id')->on('activities');
            $table->foreign('visit_type_id')
                ->references('id')->on('visit_types');
            $table->foreign('agent_id')
                ->references('id')->on('users');
            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
