<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContainerVisitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('container_visit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('container_id')->unsigned();
            $table->integer('visit_id')->unsigned();
            $table->integer('treatment_id')->unsigned();
            $table->string('sample');
            $table->smallInteger('total_amount');
            $table->smallInteger('water_amount');
            $table->smallInteger('larva_amount');

            $table->foreign('container_id')
                ->references('id')->on('containers');
            $table->foreign('visit_id')
                ->references('id')->on('visits');
            $table->foreign('treatment_id')
                ->references('id')->on('treatments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('container_visit');
    }
}
