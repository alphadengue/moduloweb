<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->date('notification_date');
            $table->date('first_signs_date');
            $table->integer('notification_city_id')->unsigned();
            $table->integer('contamination_city_id')->unsigned();
            $table->enum('result', ['positive', 'negative', 'waiting']);
            $table->string('patient_name');
            $table->date('birth_date');
            $table->string('patient_mother');
            $table->string('patient_phone');
            $table->integer('building_id')->unsigned();
            $table->integer('examination_id')->unsigned();
            $table->date('closing_date')->nullable();
            $table->text('observation')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('building_id')
                ->references('id')->on('buildings');
            $table->foreign('user_id')
                ->references('id')->on('users');
            $table->foreign('notification_city_id')
                ->references('id')->on('cities');
            $table->foreign('contamination_city_id')
                ->references('id')->on('cities');
            $table->foreign('examination_id')
                ->references('id')->on('examinations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }

}
