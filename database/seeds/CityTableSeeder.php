<?php

use AlphaDengue\City;
use AlphaDengue\State;
use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run database seeder for users table
     *
     * @return void
     */
    public function run()
    {
        $saoPauloState = State::where('name', '=', 'São Paulo')->firstOrFail();

        $cities = [
            [
                'name' => 'Aparecida',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Caçapava',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Cruzeiro',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Guaratinguetá',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Jacareí',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Lorena',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Pindamonhangaba',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Piquete',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Roseira',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'São José dos Campos',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Taubaté',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Tremembé',
                'state_id' => $saoPauloState->id
            ],
            [
                'name' => 'Ubatuba',
                'state_id' => $saoPauloState->id
            ],
        ];

        foreach ($cities as $city) {
            City::create($city);
        }
    }
}
