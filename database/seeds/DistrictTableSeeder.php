<?php

use AlphaDengue\Region;
use AlphaDengue\District;
use Illuminate\Database\Seeder;

class DistrictTableSeeder extends Seeder
{
    /**
     * Run database seeder for users table
     *
     * @return void
     */
    public function run()
    {
        $region1 = Region::where('name', '=', 'Região I')->firstOrFail();
        $region2 = Region::where('name', '=', 'Região II')->firstOrFail();
        $region3 = Region::where('name', '=', 'Região III')->firstOrFail();

        $districts = [
            [
                'name' => 'Água Quente',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Barranco',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Bosque Flamboyant',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Chácara do Visconde',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Esplanada Independência',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Esplanada Santa Terezinha',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Estiva',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Jardim Canuto Borges',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Jardim Garcez',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Jardim Mourisco',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Jardim Santa Catarina',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Parque Aeroporto',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Parque Vera Cruz',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Vila IAPI',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
            [
                'name' => 'Vila São Geraldo',
                'region_id' => $region1->id,
                'district_type' => 'urban'
            ],
        ];

        foreach ($districts as $district) {
            District::create($district);
        }
    }
}
