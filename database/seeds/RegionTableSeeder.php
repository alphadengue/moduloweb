<?php

use AlphaDengue\City;
use AlphaDengue\Region;
use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run database seeder for users table
     *
     * @return void
     */
    public function run()
    {
        $taubateCity = City::where('name', '=', 'Taubaté')->firstOrFail();

        $regions = [
            [
                'name' => 'Região I',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região II',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região III',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região IV',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região V',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região VI',
                'city_id' => $taubateCity->id
            ],
            [
                'name' => 'Região Rural',
                'city_id' => $taubateCity->id
            ],
        ];

        foreach ($regions as $region) {
            Region::create($region);
        }
    }
}
