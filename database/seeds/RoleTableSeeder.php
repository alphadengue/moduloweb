<?php

use AlphaDengue\Role;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'Administrador',
            ],
            [
                'name' => 'Diretor de Vigilância em Saúde',
            ],
            [
                'name' => 'Agente Epidemiológico',
            ],
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
