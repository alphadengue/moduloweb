<?php

use AlphaDengue\Role;
use AlphaDengue\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run database seeder for users table
     *
     * @return void
     */
    public function run()
    {
        $administrator = Role::where('name', '=', 'Administrador')->firstOrFail();

        $users = [
            [
                'name' => 'Caique de Castro',
                'email' => 'caiquecastro@outlook.com',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
                'role_id' => $administrator->id
            ],
            [
                'name' => 'Renan Alves',
                'email' => 'alvees.renan@gmail.com',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
                'role_id' => $administrator->id
            ],
            [
                'name' => 'Luis Fernando',
                'email' => 'luisfalm@gmail.com',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
                'role_id' => $administrator->id
            ],
            [
                'name' => 'Stella Zöllner',
                'email' => 'consultoriomedicord@yahoo.com.br',
                'password' => bcrypt('123456'),
                'api_token' => str_random(60),
                'role_id' => $administrator->id
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
