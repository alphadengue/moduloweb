'use strict';

;(function (win, doc, undefined) {
    var menu_toggle = doc.getElementById('menu-toggle'),
        menu_content = doc.getElementsByClassName('menu-content')[0],
        dropdown_menu = doc.getElementsByClassName('sidebar-menu-item dropdown'),
        sidebar_submenus = doc.getElementsByClassName('sidebar-submenu'),
        form_login = doc.getElementById('form-login'),
        error_list = doc.getElementById('error-list'),
        loading = doc.getElementById('loading'),
        field_user = doc.getElementById('field-user'),
        field_pass = doc.getElementById('field-password'),
        states_cb = doc.getElementById('state_id'),
        cities_cb = doc.getElementById('city_id'),
        regions_cb = doc.getElementById('region_id'),
        districts_cb = doc.getElementById('district_id'),
        streets_cb = doc.getElementById('street_id'),
        buildings_cb = doc.getElementById('building_id'),
        birth_date = doc.getElementById('birth_date'),
        patient_age = doc.getElementById('patient_age'),
        btn_add_container = doc.getElementById('btn-add-container'),
        js_links = doc.querySelectorAll('.js-link');

    if (error_list) {
        error_list.style.display = 'none';
    }

    if(btn_add_container) {
        btn_add_container.addEventListener('click', addContainer);
    }

    if (birth_date) {
        birth_date.addEventListener('change', changePatientAge);
        birth_date.addEventListener('keydown', changePatientAge);
    }

    if (js_links.length) {
        for (let i = 0; i < js_links.length; i++) {
            changeJsLink(js_links[i]);
        }
    }

    for(let i = 0; i < dropdown_menu.length; i++) {
        let link = dropdown_menu[i].getElementsByClassName("sidebar-menu-link");
        link[0].addEventListener('click', showSubMenu);
    }

    menu_toggle.addEventListener('click', toggleMenu);

    if (states_cb) {
        states_cb.addEventListener('change', loadCities);
    }
    if (cities_cb) {
        cities_cb.addEventListener('change', loadRegions);
    }
    if (regions_cb) {
        regions_cb.addEventListener('change', loadDistricts);
    }
    if (districts_cb) {
        districts_cb.addEventListener('change', loadStreets);
    }
    if (streets_cb) {
        streets_cb.addEventListener('change', loadBuildings);
    }

    if (form_login) {
        form_login.addEventListener('submit', submitLogin);
    }

    function toggleMenu() {
        menu_content.classList.toggle('expanded');
    }

    function changeJsLink(element) {
        let dataLink = element.dataset.link;
        element.setAttribute('href', dataLink);
    }

    function submitLogin(e) {
        e.preventDefault();

        if (field_pass.value == '' || field_user.value == '') {
            error_list.style.display = 'block';
            error_list.innerHTML = '<p class="error-item">Preencha os campos usuário e senha</p>';
            return false;
        }

        error_list.style.display = 'none';
        form_login.style.display = 'none';
        loading.style.display = 'block';
        error_list.innerHTML = '';

        request({
            method: 'POST',
            pathname: form_login.getAttribute('action'),
            body: {
                email: field_user.value,
                password: field_pass.value
            }
        }, function(error, response) {
            if(error) {
                reportError(error);
            } else {
                if (response.status == 'success') {
                    redirect(response.redirectPath);
                } else {
                    error_list.style.display = 'block';
                    form_login.style.display = 'block';
                    loading.style.display = 'none';

                    //for (var i = 0; i < response.errors.length; i++) {
                    for (let error_item in response.errors) {
                        error_list.innerHTML += '<p class="error-item">' + error_item + '</p>';
                    }

                    if (response.redirectPath) {
                        redirect(response.redirectPath, 1000);
                    }
                }
            }
        }, true);
    }

    function loadCities() {
        let el = states_cb,
            state = el.value;

        if (state) {
            getJSONToSelect("/admin/states/" + state + "/cities", cities_cb);
        }
    }

    function loadRegions() {
        let el = cities_cb,
            city = el.value;

        if (city) {
            getJSONToSelect("/admin/cities/" + city + "/regions", regions_cb);
        }
    }

    function loadDistricts() {
        let el = regions_cb,
            region = el.value;

        if (region) {
            getJSONToSelect("/admin/regions/" + region + "/districts", districts_cb);
        }
    }

    function loadStreets() {
        let el = districts_cb,
            district = el.value;

        if (district) {
            getJSONToSelect("/admin/districts/" + district + "/streets", streets_cb);
        }
    }

    function loadBuildings() {
        let el = streets_cb,
            street = el.value;

        if (street) {
            getJSONToSelect("/admin/streets/" + street + "/buildings", buildings_cb, 'street_number');
        }
    }

    function redirect(url, timer) {
        if (timer) {
            win.setTimeout(redirect, timer, url);
        } else {
            win.location.replace(url);
        }
    }

    function clearElement(select) {
        if (!select) return;
        let node = select.firstChild;
        while (node) {
            select.removeChild(node);
            node = select.firstChild;
        }
    }

    function getJSONToSelect(url, select, label) {
        if (!select) return;
        clearElement(select);

        label = label || 'name';

        request({
            pathname: url
        }, function(error, response) {
            if(error) {
                alert(error.toString());
            } else {
                let option = null;

                if (response.data.length == 0) {
                    option = doc.createElement('option');
                    option.value = '';
                    option.innerHTML = 'Nenhuma opção disponível';
                    option.disabled = true;
                    option.selected = true;
                    select.appendChild(option);
                } else {
                    option = doc.createElement('option');
                    option.innerHTML = 'Selecione';
                    option.value = '';
                    option.disabled = true;
                    option.selected = true;
                    select.appendChild(option);
                    for (let i = 0; i < response.data.length; i++) {
                        option = doc.createElement('option');
                        option.value = response.data[i]['id'];
                        option.innerHTML = response.data[i][label];
                        select.appendChild(option);
                    }
                }
            }
        }, true);
    }

    function buildStatusChart(notificationsPerStatus) {
        request({
            pathname: '/admin/notifications/status'
        }, function(errors, response) {
            new Chart(notificationsPerStatus, {
                type: 'pie',
                data: {
                    labels: Object.keys(response),
                    datasets: [{
                        label: 'Notificações por status',
                        data: Object.keys(response).map(key => response[key]),
                        backgroundColor: [
                            '#A63446',
                            '#0C6291',
                            '#CECECE'
                        ],
                    }]
                }
            });
        });
    }

    function buildAgeChart(notificationsPerAge) {
        request({
            pathname: '/admin/notifications/age'
        }, function(errors, response) {
            let ageRanges = [{
                label: '0 a 1 ano',
                lower: 0,
                upper: 1
            }, {
                label: '2 a 4 anos',
                lower: 2,
                upper: 4
            }, {
                label: '5 a 9 anos',
                lower: 5,
                upper: 9
            }, {
                label: '10 a 14 anos',
                lower: 10,
                upper: 14
            }, {
                label: '15 a 19 anos',
                lower: 15,
                upper: 19
            }, {
                label: '20 a 29 anos',
                lower: 20,
                upper: 29
            }, {
                label: '30 a 39 anos',
                lower: 30,
                upper: 39
            }, {
                label: '40 a 49 anos',
                lower: 40,
                upper: 49
            }, {
                label: '50 a 59 anos',
                lower: 50,
                upper: 59
            }, {
                label: '60 a 69 anos',
                lower: 60,
                upper: 69
            }, {
                label: '70 a 79 anos',
                lower: 70,
                upper: 79
            }, {
                label: '80 anos ou mais',
                lower: 80,
                upper: 200
            }];

            new Chart(notificationsPerAge, {
                type: 'bar',
                data: {
                    labels: Object.keys(ageRanges).map(key => ageRanges[key].label),
                    datasets: [{
                        label: 'Aguardando resultado',
                        data: aggregateByAge(response, ageRanges, 'waiting'),
                        backgroundColor: '#333'
                    }, {
                        label: 'Positivo',
                        data: aggregateByAge(response, ageRanges, 'positive'),
                        backgroundColor: '#0C6291'
                    }, {
                        label: 'Negativo',
                        data: aggregateByAge(response, ageRanges, 'negative'),
                        backgroundColor: '#A63446'
                    }]
                }
            });
        });
    }

    function buildRegionChart(notificationsPerRegion) {
        request({
            pathname: '/admin/notifications/regions'
        }, function(errors, response) {

            let regions = [
                'Região I',
                'Região II',
                'Região III',
                'Região IV',
                'Região V',
                'Região VI'
            ];

            new Chart(notificationsPerRegion, {
                type: 'bar',
                data: {
                    labels: regions,
                    datasets: [{
                        label: 'Aguardando resultado',
                        data: aggregateByRegion(response, regions, 'waiting'),
                        backgroundColor: '#333'
                    }, {
                        label: 'Positivo',
                        data: aggregateByRegion(response, regions, 'positive'),
                        backgroundColor: '#0C6291'
                    }, {
                        label: 'Negativo',
                        data: aggregateByRegion(response, regions, 'negative'),
                        backgroundColor: '#A63446'
                    }]
                }
            });
        });
    }

    function buildCharts() {
        let notificationsPerStatus = doc.querySelector('#general-chart');

        if (notificationsPerStatus) {
            buildStatusChart(notificationsPerStatus);
        }

        let notificationsPerAge = doc.querySelector('#age-chart');

        if (notificationsPerAge) {
            buildAgeChart(notificationsPerAge);
        }

        let notificationsPerRegion = doc.querySelector('#region-chart');

        if (notificationsPerRegion) {
            buildRegionChart(notificationsPerRegion);
        }
    }

    function changePatientAge() {
        let date = this.value;

        let age = calcAge(date);

        if (age == null) {
            patient_age.value = '';
        } else {
            patient_age.value = age.years + ' anos e ' + age.months + ' meses';
        }
    }

    function calcAge(date_str) {
        let today = new Date();
        let date = new Date(date_str);

        if (isNaN(date.getTime()) || date.getTime() > today.getTime()) {
            return null;
        }

        let age = {
            years: 0,
            months: 0
        };

        age.years = today.getUTCFullYear() - date.getUTCFullYear();

        if (today.getUTCMonth() < date.getUTCMonth() || today.getUTCMonth() == date.getUTCMonth() && today.getUTCDate() < date.getUTCDate()) {
            age.years--;
        }

        age.months = today.getUTCMonth() - date.getUTCMonth();

        if(today.getUTCDate() < date.getUTCDate()) {
            age.months--;
        }

        if (age.months < 0) {
            age.months += 12;
        }

        if (age.years < 0 || age.months < 0) {
            return null;
        }

        return age;
    }

    function showSubMenu(e) {
        e.preventDefault();

        let subMenu = this.parentNode.getElementsByClassName("sidebar-submenu")[0];

        for(let i = 0; i < dropdown_menu.length; i++) {
            let link = dropdown_menu[i].getElementsByClassName("sidebar-menu-link")[0];
            if(link != this) {
                link.classList.remove('sidebar-active-item');
            }
        }

        this.classList.toggle('sidebar-active-item');

        for(let i = 0; i < sidebar_submenus.length; i++) {
            if(sidebar_submenus[i] != subMenu) {
                sidebar_submenus[i].classList.remove('active');
            }
        }

        subMenu.classList.toggle('active');
    }

    function request(options, callback, ajax) {
        let method = options.method || 'GET';
        let headers = new Headers();
        let body = JSON.stringify(options.body);
        let metaTagToken = doc.querySelector('meta[name="csrf-token"]');

        headers.set('Content-Type', 'application/json');
        if (metaTagToken) {
            headers.append('X-CSRF-TOKEN', metaTagToken.getAttribute('content'));
        }
        if(ajax) {
            headers.append('X-Requested-With', 'XMLHttpRequest');
        }

        var request = new Request(options.pathname, {
            method: method,
            body: body,
            headers: headers,
            credentials: 'include'
        });

        fetch(request).then(function(response) {
            if (response.status >= 200 && response.status < 300) {
                return Promise.resolve(response)
            }

            return Promise.reject(new Error(response.statusText))
        }).then(function(response) {
            return response.json();
        }).then(function(response) {
            callback(null, response);
        }).catch(function(err) {
            callback(err);
        });
    }

    function reportError(error) {
        if(error) {
            error_list.innerHTML = '<p class="error-item">' + error.toString() + '</p>';
            error_list.style.display = 'block';
            form_login.style.display = 'block';
            loading.style.display = 'none';
        }
    }

    function initializeBuildingMap() {
        let buildingMap = document.querySelector("#building-map");
        if (! buildingMap) {
            return;
        }

        let latitude = buildingMap.getAttribute('data-latitude');
        let longitude = buildingMap.getAttribute('data-longitude');
        let center = new google.maps.LatLng(latitude, longitude);

        let map = new google.maps.Map(buildingMap, {
            center: center,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        let marker = new google.maps.Marker({
            position: center,
            map: map,
            draggable: true,
            title: 'Hello World!'
        });
    }
    function initializeVisitsHeatMap() {
        let heatmap = document.getElementById("heatmap-visits");
        if(!heatmap) {
            return;
        }

        let heatmapData = [],
            taubate = new google.maps.LatLng(-23.014013, -45.55125),
            map = new google.maps.Map(heatmap, {
                center: taubate,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            });

        request({
            pathname: "/visits/points" + win.location.search
        }, function(error, response) {
            for(let i = 0; i < response.length; i++) {
                heatmapData.push(new google.maps.LatLng(parseFloat(response[i].latitude), parseFloat(response[i].longitude)));
            }
            let heatmap = new google.maps.visualization.HeatmapLayer({
                data: heatmapData
            });
            let gradient = [
                'rgba(0, 255, 255, 0)',
                'rgba(191, 100, 0, 1)',
                'rgba(230, 200, 0, 1)',
                'rgba(255, 255, 0, 1)',
                'rgba(255, 230, 0, 1)',
                'rgba(255, 204, 0, 1)',
                'rgba(255, 160, 0, 1)',
                'rgba(204, 60, 0, 1)',
                'rgba(180, 60, 0, 1)',
                'rgba(63, 10, 0, 1)',
                'rgba(63, 0, 0, 1)',
                'rgba(127, 0, 0, 1)',
                'rgba(191, 0, 0, 1)',
                'rgba(255, 0, 0, 1)'
            ];
            heatmap.setOptions({
                gradient: gradient
            });
            heatmap.setMap(map);
        }, true);
    }

    function initializeHeatMap() {
        let heatmap = document.getElementById("heatmap");
        if(!heatmap) {
            return;
        }

        let heatmapData = [],
            taubate = new google.maps.LatLng(-23.014013, -45.55125),
            map = new google.maps.Map(heatmap, {
                center: taubate,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false
            });

        request({
            pathname: "/notifications/points" + win.location.search
        }, function(error, response) {
            for(let i = 0; i < response.length; i++) {
                heatmapData.push(new google.maps.LatLng(parseFloat(response[i].latitude), parseFloat(response[i].longitude)));
            }
            let heatmap = new google.maps.visualization.HeatmapLayer({
                data: heatmapData
            });
            let gradient = [
                'rgba(0, 255, 255, 0)',
                'rgba(191, 100, 0, 1)',
                'rgba(230, 200, 0, 1)',
                'rgba(255, 255, 0, 1)',
                'rgba(255, 230, 0, 1)',
                'rgba(255, 204, 0, 1)',
                'rgba(255, 160, 0, 1)',
                'rgba(204, 60, 0, 1)',
                'rgba(180, 60, 0, 1)',
                'rgba(63, 10, 0, 1)',
                'rgba(63, 0, 0, 1)',
                'rgba(127, 0, 0, 1)',
                'rgba(191, 0, 0, 1)',
                'rgba(255, 0, 0, 1)'
            ];
            heatmap.setOptions({
              gradient: gradient
            });
            heatmap.setMap(map);
        }, true);
    }
    google.maps.event.addDomListener(win, "load", initializeHeatMap);

    google.maps.event.addDomListener(win, "load", initializeVisitsHeatMap);

    google.maps.event.addDomListener(win, "load", initializeBuildingMap);

    buildCharts();

    function addContainer(e) {
        let row = e.target.parentNode.parentNode;
        let newRow = row.cloneNode(true);
        let inputs = row.getElementsByTagName('input');
        for(let i = 0; i < inputs.length; i++) {
            inputs[i].value = '';
        }
        let selects = row.getElementsByTagName('select');
        for(let i = 0; i < selects.length; i++) {
            selects[i].selectedIndex = 0;
        }
        var button = newRow.getElementsByClassName('action-symbol');
        button[0].innerHTML = '-';
        button[0].classList.remove('action-symbol');
        var table = doc.getElementById('container-row');
        table.appendChild(newRow);
    }

    function aggregateByAge(data = [], range, status) {
        let result = [];

        range.forEach(function (age) {
            let filtered = data.filter(item => {
                return (
                    item.age <= age.upper &&
                    item.age >= age.lower &&
                    item.result == status
                );
            });

            let total = filtered.reduce((previous, current) => {
                return previous + current.total;
            }, 0);

            result.push(total);
        });

        return result;
    }

    function aggregateByRegion(data = [], regions, status) {
        let result = [];

        regions.forEach(function(region) {
            let filtered = data.filter(item => {
                return (
                    item.name == region &&
                    item.result == status
                );
            });

            let total = filtered.reduce((previous, current) => {
                return previous + current.total;
            }, 0);

            result.push(total);
        });

        return result;
    }
})(window, document);
