Alpha Dengue
============

Alpha Dengue é um projeto resultado do Trabalho de Graduação Interdisciplinar dos alunos Caíque de Castro Soares da Silva e Renan Alves do Curso de Engenharia de Computação da Univeridade de Taubaté.
Esse projeto é dividido em dois módulos: o aplicativo web e o aplicativo nativo para Android. Esse projeto é o módulo web.
Orietador: Prof. Dr. Luis Fernando de Almeida

Requirements
------------
* Linux
* PHP (>= 5.6.4)
* OpenSSL PHP Extension
* Mbstring PHP Extension
* PostgreSQL (>= 9.5)
* Git
* Composer
* NPM
* Bower

Installation
------------
1. Clone this repository: `git clone https://caiquecssilva@bitbucket.org/alphadengue/moduloweb.git AlphaDengue`
2. Change to the project folder: `cd AlphaDengue`
3. Copy the content from .env.example to .env
4. Regenerate a new app key for security reasons
5. Install composer dependencies: `composer install`
6. Install NPM dependencies: `npm install`
7. Install bower dependencies: `bower install`
8. Run gulp to compile the scss and javascript: `gulp watch`
9. Serve the application on the PHP development server: `php artisan serve`
9. Browse to: http://localhost:8000/

Notes
-----

