@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Atividade</h2>
    <a href="/admin/activities">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('activities.store') }}">
            @include('activities._form')
        </form>
    </div>
@stop