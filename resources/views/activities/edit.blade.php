@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Atividade</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('activities.update', [ $activity->id ]) }}">
            {{ method_field('PUT') }}
            @include('activities._form')
        </form>
    </div>
@stop