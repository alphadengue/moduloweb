@extends('admin')

@section('content')
    <h2 class="dashboard-title">Atividades</h2>
    <a href="/admin/activities/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
                <tr>
                    <th>Descrição</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities as $activity)
                    <tr>
                        <td>{{ $activity->name }}</td>
                        <td>
                            <a href="{{ action('ActivitiesController@show', $activity->id) }}"
                               class="btn btn-primary btn-xs"
                            >
                                Ver
                            </a>
                            <a href="{{ action('ActivitiesController@edit', $activity->id) }}"
                               class="btn btn-info btn-xs"
                            >
                                Editar
                            </a>
                            <form method="POST"
                                  class="form--inline"
                                  action="{{ action('ActivitiesController@destroy', [ $activity->id ]) }}"
                            >
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-xs">
                                    Excluir
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $activities->links() }}
    </div>
@stop
