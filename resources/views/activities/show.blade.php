@extends('admin')

@section('content')
    <h2 class="dashboard-title">Atividade: {{$activities->name}}</h2>
    <div class="dash-content">
        <p>Criada em: {{$activities->created_at->format('d-m-Y')}}</p>
        <p>Atualizada em: {{$activities->updated_at}}</p>
        <h3>Visitas</h3>
        <table>
            <thead>
                <tr>
                    <th>Data da Visita</th>
                    <th>Tipo de Visita</th>
                    <th>Imóvel</th>
                    <th>Agente</th>
                </tr>
            </thead>
            <tbody>
                @foreach($activities->visits as $visit)
                    <td>{{ $visit->visited_at }}</td>
                    <td>{{ $visit->building->street->name }}, {{ $visit->building->street_number }}</td>
                    <td>{{ $visit->visit_type->name }}</td>
                    <td>{{ $visit->activity->name }}</td>
                @endforeach
            </tbody>
        </table>
    </div>
@stop