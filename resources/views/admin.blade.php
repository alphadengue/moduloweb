<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Alpha Dengue - Vigilância Epidemiológica de Taubaté</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round%7COpen+Sans">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="icon" href="{{ url('/favicon.ico')  }}">
</head>
<body class="site admin">
    <header class="header">
        <h1 class="brand">
            <a href="{{ action('AdminController@index') }}" class="brand-link">
                <img src="{{ asset('images/alpha.png') }}" alt="Alpha Dengue" class="brand-image">
                <span class="brand-description">ALPHA DENGUE</span>
            </a>
        </h1>
        <nav class="main-menu">
            <a href="#" id="menu-toggle">Menu</a>
            <ul class="menu-content">
                <li class="menu-item dropdown">
                    <a href="#" class="menu-link">Olá, {{ auth()->user()->name }}</a>
                    <ul class="submenu">
                        <li class="submenu-item">
                            <a href="{{ action('SettingsController@index') }}" class="menu-link">Configurações</a>
                        </li>
                        <li class="submenu-item">
                            <form method="POST" action="{{ action('Auth\LoginController@logout') }}">
                                {{ csrf_field() }}
                                <button class="menu-link">Sair</button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </header>
    <div class="admin-container">
        @include('admin.sidebar')
        <div class="dashboard">
            @yield('content')
        </div>
    </div>
    <footer class="footer">
        <p>
            Alpha Dengue - Vigilância Epidemiológica de Taubaté - Produzido por alunos da UNITAU.<br>
            Todos os direitos reservados - &copy; {{ date("Y") }}
        </p>
    </footer>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.0/Chart.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnEAcdFsRATyHvNghiOrxmFlOyQpixTdk&libraries=geometry,visualization"></script>
    <script src="/js/main.js"></script>
</body>
</html>