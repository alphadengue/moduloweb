@extends('admin')

@section('content')
	<div class="tile tile-9">
		<h2 class="dashboard-title">Painel de Controle</h2>
	</div>
	<form action="/admin">
	<div class="tile tile-3">
		<input type="date" name="date-from" value="{{ request('date-from') }}" required>
	</div>
	<div class="tile tile-3">
		<input type="date" name="date-to" value="{{ request('date-to') }}" required>
	</div>
	<div class="tile tile-3">
		<input type="submit" class="btn btn-default" value="Filtrar">
	</div>
	</form>
	<div class="dash-content">
	    <div class="tile tile-18">
	        <h3 class="dashboard-title">Mapa de Notificações</h3>
	        <div id="heatmap" class="heatmap"></div>
	    </div>
		<div class="tile tile-18">
			<h3 class="dashboard-title">Mapa de Visitas</h3>
			<div id="heatmap-visits" class="heatmap"></div>
		</div>
	    {{--<div class="tile tile-18">--}}
	        {{--<h3 class="dashboard-title">Casos Por Região</h3>--}}
	        {{--<canvas id="report-chart" class="chart"></canvas>--}}
	    {{--</div>--}}
	</div>
@stop