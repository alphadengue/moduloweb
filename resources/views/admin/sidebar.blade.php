<div class="sidebar">
    <ul class="sidebar-menu">
        <li class="sidebar-menu-item{{ $currentPage === 'home' ? ' active' : '' }}">
            <a class="sidebar-menu-link" href="/admin">Painel</a>
        </li>
        <li class="sidebar-menu-item dropdown">
            <a href="#" class="sidebar-menu-link{{ $visitsMenuEnabled ? ' sidebar-active-item': '' }}">
                Visitas
            </a>
            <ul class="sidebar-submenu{{ $visitsMenuEnabled ? ' active': '' }}">
                <li class="sidebar-menu-item{{ preg_match('/visits\.[^c].*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/visits">Todas visitas</a>
                </li>
                <li class="sidebar-menu-item{{ $currentPage === 'visits.create' ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/visits/create">Nova visita</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/visit-types\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/visit-types">Tipos de Visita</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/activities\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/activities">Atividades</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/larvae\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/larvae">Larvas</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/containers\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/containers">Recipientes</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/treatments\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/treatments">Tratamentos</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/building-statuses\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/building-statuses">Situações de Imóveis</a>
                </li>
            </ul>
        </li>
        <li class="sidebar-menu-item dropdown">
            <a href="#" class="sidebar-menu-link{{ $notificationsMenuEnabled ? ' sidebar-active-item': '' }}">
                Notificações
            </a>
            <ul class="sidebar-submenu{{ $notificationsMenuEnabled ? ' active': '' }}">
                <li class="sidebar-menu-item{{ preg_match('/notifications\.[^c].*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/notifications">Todas notificações</a>
                </li>
                <li class="sidebar-menu-item{{ $currentPage === 'notifications.create' ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/notifications/create">Nova notificação</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/examinations\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/examinations">Exames</a>
                </li>
            </ul>
        </li>
        <li class="sidebar-menu-item dropdown">
            <a href="#" class="sidebar-menu-link{{ $locationsMenuEnabled ? ' sidebar-active-item': '' }}">
                Localidades
            </a>
            <ul class="sidebar-submenu{{ $locationsMenuEnabled ? ' active': '' }}">
                <li class="sidebar-menu-item{{ preg_match('/states\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/states">Unidades Federativas</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/cities\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/cities">Municípios</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/regions\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/regions">Regiões</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/districts\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/districts">Bairros</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/streets\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/streets">Logradouros</a>
                </li>
                <li class="sidebar-menu-item{{ $currentPage === 'buildings.index' ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/buildings">Imóveis</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/building-types\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/building-types">Tipos de Imóveis</a>
                </li>
            </ul>
        </li>
        <li class="sidebar-menu-item dropdown">
            <a href="#" class="sidebar-menu-link{{ $aclMenuEnabled ? ' sidebar-active-item': '' }}">
                Usuários
            </a>
            <ul class="sidebar-submenu{{ $aclMenuEnabled ? ' active': '' }}">
                <li class="sidebar-menu-item{{ $currentPage === 'users.index' ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/users">Todos usuários</a>
                </li>
                <li class="sidebar-menu-item{{ $currentPage === 'users.create' ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/users/create">Novo usuário</a>
                </li>
                <li class="sidebar-menu-item{{ preg_match('/roles\..*/', $currentPage) ? ' active' : '' }}">
                    <a class="sidebar-menu-link" href="/admin/roles">Gerenciar permissões</a>
                </li>
            </ul>
        </li>
    </ul>
</div>