@extends('public')

@section('content')
<div class="page">
    <h2 class="page-title">Login</h2>
    @include('errors.list')
    <form class="formContato" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <label for="field-usuario">Usuário</label>
        <input type="email" id="field-usuario" name="email" value="{{ old('email') }}" required autofocus>
        <label for="field-senha">Senha</label>
        <input type="password" id="field-senha" name="password" required>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="remember"> Manter conectado
            </label>
        </div>
        <button type="submit" class="btn btn-success">Entrar</button>
        <a href="{{ url('/password/reset') }}">Esqueci a senha</a>
    </form>
</div>
@endsection
