@extends('public')

<!-- Main Content -->
@section('content')
<div class="page">
    <h2 class="page-title">Recuperar senha</h2>
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="form-horizontal" method="POST" action="{{ url('/password/email') }}">
        {{ csrf_field() }}

        <label for="email" class="col-md-4 control-label">E-Mail Address</label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

        <button type="submit" class="btn btn-primary">
            Send Password Reset Link
        </button>
    </form>
</div>
@endsection
