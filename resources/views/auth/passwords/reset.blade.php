@extends('public')

@section('content')
<div class="page">
    <h2 class="page-title">Restaurar senha</h2>
    @include('errors.list')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">

        <label for="email">E-Mail Address</label>
        <input id="email" type="email" name="email" value="{{ $email or old('email') }}" required autofocus>

        <label for="password" class="col-md-4 control-label">Password</label>
        <input id="password" type="password" name="password" required>

        <label for="password-confirm">Confirm Password</label>
        <input id="password-confirm" type="password" name="password_confirmation" required>

        <button type="submit" class="btn btn-primary">
            Reset Password
        </button>
    </form>
</div>
@endsection
