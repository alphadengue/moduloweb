@extends('public')

@section('content')
<div class="page">
    <h2 class="page-title">Cadastro</h2>
    @include('errors.list')
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
        {{ csrf_field() }}

        <label for="field-name">Nome</label>
        <input id="field-name" type="text" name="name" value="{{ old('name') }}" required autofocus>

        <label for="field-email">Endereço de e-mail</label>
        <input id="field-email" type="email" name="email" value="{{ old('email') }}" required>

        <label for="field-password">Senha</label>
        <input id="field-password" type="password" name="password" required>


        <label for="password-confirm">Confirmar senha</label>
        <input id="password-confirm" type="password" name="password_confirmation" required>

        <button type="submit" class="btn btn-success">
            Register
        </button>
    </form>
</div>
@endsection
