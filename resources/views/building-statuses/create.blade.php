@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Situação de Imóvel</h2>
    <a href="/admin/building-statuses">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('building-statuses.store') }}">
            @include('building-statuses._form')
        </form>
    </div>
@stop