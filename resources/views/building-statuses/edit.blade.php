@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Situação de Imóvel</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('building-statuses.update', [ $status->id ]) }}">
            {{ method_field('PUT') }}
            @include('building-statuses._form')
        </form>
    </div>
@stop