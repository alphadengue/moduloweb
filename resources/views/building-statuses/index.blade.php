@extends('admin')

@section('content')
    <h2 class="dashboard-title">Situações de Imóveis</h2>
    <a href="/admin/building-statuses/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($building_statuses as $status)
                <tr>
                    <td>{{ $status->name }}</td>
                    <td>
                        <a href="{{ action('BuildingStatusesController@show', $status->id) }}"
                           class="btn btn-primary btn-xs"
                        >
                            Ver
                        </a>
                        <a href="{{ action('BuildingStatusesController@edit', $status->id) }}"
                           class="btn btn-info btn-xs"
                        >
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('BuildingStatusesController@destroy', [ $status->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
