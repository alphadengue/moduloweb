@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Tipo de Imóvel</h2>
    <a href="/admin/building-types">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('building-types.store') }}">
            @include('building-types._form')
        </form>
    </div>
@stop