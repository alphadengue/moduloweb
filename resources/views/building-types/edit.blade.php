@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Tipo de Imóvel</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('building-types.update', [ $building_type->id ]) }}">
            {{ method_field('PUT') }}
            @include('building-types._form')
        </form>
    </div>
@stop