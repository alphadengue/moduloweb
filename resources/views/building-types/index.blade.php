@extends('admin')

@section('content')
    <h2 class="dashboard-title">Tipos de Imóveis</h2>
    <a href="/admin/building-types/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($building_types as $type)
                <tr>
                    <td>{{ $type->id }}</td>
                    <td>{{ $type->name }}</td>
                    <td>
                        <a href="{{ action('BuildingTypesController@show', $type->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('BuildingTypesController@edit', $type->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('BuildingTypesController@destroy', [ $type->id ]) }}"
                            >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
