{{ csrf_field() }}

<label for="state_id">Estado</label>
<select name="state_id" id="state_id">
    <option disabled selected>Selecione</option>
    @foreach ($states as $id => $state)
        <option value="{{ $id }}"
                @if ($id == old('state_id', $building->street->district->region->city->state_id ?? ''))
                selected
                @endif
        >
            {{ $state }}
        </option>
    @endforeach
</select>

<label for="city_id">Cidade</label>
<select name="city_id" id="city_id">
    <option disabled selected>Selecione</option>
    @foreach ($cities as $id => $city)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $building->street->district->region->city_id ?? ''))
                selected
                @endif
        >
            {{ $city }}
        </option>
    @endforeach
</select>

<label for="region_id">Região</label>
<select name="region_id" id="region_id">
    <option disabled selected>Selecione</option>
    @foreach ($regions as $id => $region)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $building->street->district->region_id))
                selected
                @endif
        >
            {{ $region }}
        </option>
    @endforeach
</select>

<label for="district_id">Bairro</label>
<select name="district_id" id="district_id">
    <option disabled selected>Selecione</option>
    @foreach ($districts as $id => $district)
        <option value="{{ $id }}"
                @if ($id == old('district_id', $building->street->district_id))
                selected
                @endif
        >
            {{ $district }}
        </option>
    @endforeach
</select>

<label for="street_id">Logradouro</label>
<select name="street_id" id="street_id">
    <option disabled selected>Selecione</option>
    @foreach ($streets as $id => $street)
        <option value="{{ $id }}"
                @if ($id == old('district_id', $building->street_id))
                selected
                @endif
        >
            {{ $street }}
        </option>
    @endforeach
</select>

<label for="street_number">Número:</label>
<input type="text" name="street_number" id="street_number" value="{{ old('postcode', $building->street_number) }}">

<label for="additional_address_details">Complemento:</label>
<input type="text" name="additional_address_details" id="additional_address_details" value="{{ old('postcode', $building->additional_address_details) }}">

<label for="building_type_id">Tipo de Domicílio:</label>
<select name="building_type_id" id="building_type_id">
    <option disabled selected>Selecione</option>
    @foreach ($building_types as $id => $building_type)
        <option value="{{ $id }}"
                @if ($id == old('building_type_id', $building->building_type_id ?? ''))
                selected
                @endif
        >
            {{ $building_type }}
        </option>
    @endforeach
</select>

<button type="submit" class="btn btn-primary">
    Salvar
</button>
