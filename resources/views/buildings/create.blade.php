@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Imóvel</h2>
    <a href="/admin/buildings">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('buildings.store') }}">
            @include('buildings._form')
        </form>
    </div>
@stop