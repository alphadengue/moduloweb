@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Imóvel</h2>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('buildings.update', [ $building->id ]) }}">
            {{ method_field('PUT') }}
            @include('buildings._form')
        </form>
    </div>
@stop
