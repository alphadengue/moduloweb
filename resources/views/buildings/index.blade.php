@extends('admin')

@section('content')
    <h2 class="dashboard-title">Imóveis</h2>
    <a href="/admin/buildings/create">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Logradouro</th>
                <th>Número</th>
                <th>Complemento</th>
                <th>Bairro</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($buildings as $building)
                <tr>
                    <td>{{ $building->street->name }}</td>
                    <td>{{ $building->street_number }}</td>
                    <td>{{ $building->additional_address_details }}</td>
                    <td>{{ $building->street->district->name }}</td>
                    <td>
                        <a href="{{ action('BuildingsController@show', $building->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('BuildingsController@edit', $building->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('BuildingsController@destroy', [ $building->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $buildings->render() !!}
    </div>
@stop
