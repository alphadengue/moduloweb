@extends('admin')

@section('content')
    <h2 class="dashboard-title">
        Imóvel: {{ $building->street->name }}, {{ $building->street_number }} - {{ $building->street->district->name }}</h2>
    <div class="dash-content">
        <div id="building-map"
             data-latitude="{{$building->latitude}}"
             data-longitude="{{$building->longitude}}"
             class="building-map"
        >
            <div class="alert alert-warning">Habilite o javascript para visualizar o mapa.</div>
        </div>
        <h3>Notificações</h3>
        <table>
            <thead>
            <tr>
                <th>Número</th>
                <th>Paciente</th>
                <th>Data da Notificação</th>
                <th>Data dos Primeiros Sintomas</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($building->notifications as $notification)
                <tr>
                    <td>{{ $notification->code }}</td>
                    <td>{{ $notification->patient_name }}</td>
                    <td>{{ \Carbon\Carbon::parse($notification->notification_date)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($notification->fist_signs_date)->format('d/m/Y') }}</td>
                    <td>
                        <a href="{{ action('NotificationsController@show', $notification->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <h3>Visitas</h3>
        <table>
            <thead>
            <tr>
                <th>Data da Visita</th>
                <th>Imóvel</th>
                <th>Tipo de visita</th>
                <th>Atividade</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($building->visits as $visit)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($visit->visited_at)->format("d/m/Y") }}</td>
                    <td>{{ $visit->building->street->name }}, {{ $visit->building->street_number }}</td>
                    <td>{{ $visit->visit_type->name }}</td>
                    <td>{{ $visit->activity->name }}</td>
                    <td>
                        <a href="{{ action('VisitsController@show', $visit->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop