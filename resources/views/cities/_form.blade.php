{{ csrf_field() }}

<label for="state_id">Estado</label>
<select name="state_id" id="state_id">
    <option disabled selected>Selecione</option>
    @foreach ($states as $id => $state)
        <option value="{{ $id }}"
                @if ($id == old('state_id', $city->state_id))
                selected
                @endif
        >
            {{ $state }}
        </option>
    @endforeach
</select>

<label for="name">Nome</label>
<input type="text" name="name" id="name" value="{{ old('name', $city->name) }}">

<button class="btn btn-primary" type="submit">Salvar</button>