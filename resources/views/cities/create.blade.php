@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Município</h2>
    <a href="/admin/cities">Ver todas</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('cities.store') }}">
            @include('cities._form')
        </form>
    </div>
@stop