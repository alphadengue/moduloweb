@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Município</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('cities.update', [ $city->id ]) }}">
            {{ method_field('PUT') }}
            @include('cities._form')
        </form>
    </div>
@stop