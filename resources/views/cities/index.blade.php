@extends('admin')

@section('content')
    <h2 class="dashboard-title">Municípios</h2>
    <a href="{{ route('cities.create') }}">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="GET" action="{{ route('cities.index') }}">
            @include('partials._search')
        </form>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Estado</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cities as $city)
                <tr>
                    <td>{{ $city->name }}</td>
                    <td>{{ $city->state->name }}</td>
                    <td>
                        <a href="{{ action('CitiesController@show', $city->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('CitiesController@edit', $city->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('CitiesController@destroy', [ $city->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $cities->appends(request()->query())->render() }}
    </div>
@stop
