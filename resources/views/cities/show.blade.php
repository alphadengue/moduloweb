@extends('admin')

@section('content')
    <h2 class="dashboard-title">Município: {{ $city->name }}</h2>
    <div class="dash-content">

        <h3>Regiões</h3>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($city->regions as $region)
                <tr>
                    <td>{{ $region->name }}</td>
                    <td>
                        <a href="{{ action('RegionsController@show', $region->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <h3>Relatórios</h3>
        <form action="">
            <label for="from-date">Desde:</label>
            <input type="date" id="from-date">

            <label for="to-date">Até:</label>
            <input type="date" id="to-date">

            <button class="btn btn-success">Atualizar</button>
        </form>

        <div class="report-box">
            <h4>Geral</h4>
            <div class="report-graph">
                <canvas id="general-chart" class="chart"></canvas>
            </div>
            <div class="report-table"></div>
        </div>

        <div class="report-box">
            <h4>Por faixa etária</h4>
            <div class="report-graph">
                <canvas id="age-chart" class="chart"></canvas>
            </div>
            <div class="report-table"></div>
        </div>

        <div class="report-box">
            <h4>Por região</h4>
            <div class="report-graph">
                <canvas id="region-chart" class="chart"></canvas>
            </div>
            <div class="report-table"></div>
        </div>
    </div>
@stop