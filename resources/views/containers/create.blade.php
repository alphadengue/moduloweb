@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Recipiente</h2>
    <a href="/admin/containers">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('containers.store') }}">
            @include('containers._form')
        </form>
    </div>
@stop