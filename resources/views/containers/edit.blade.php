@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Recipiente</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('containers.update', [ $container->id ]) }}">
            {{ method_field('PUT') }}
            @include('containers._form')
        </form>
    </div>
@stop