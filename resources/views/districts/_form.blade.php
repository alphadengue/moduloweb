{{ csrf_field() }}

<label for="state_id">Estado</label>
<select name="state_id" id="state_id">
    <option disabled selected>Selecione</option>
    @foreach ($states as $id => $state)
        <option value="{{ $id }}"
                @if ($id == old('state_id', $district->region->city->state_id ?? ''))
                selected
                @endif
        >
            {{ $state }}
        </option>
    @endforeach
</select>

<label for="city_id">Cidade</label>
<select name="city_id" id="city_id">
    <option disabled selected>Selecione</option>
    @foreach ($cities as $id => $city)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $district->region->city_id ?? ''))
                selected
                @endif
        >
            {{ $city }}
        </option>
    @endforeach
</select>

<label for="region_id">Região</label>
<select name="region_id" id="region_id">
    <option disabled selected>Selecione</option>
    @foreach ($regions as $id => $region)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $district->region_id))
                selected
                @endif
        >
            {{ $region }}
        </option>
    @endforeach
</select>

<label for="name">Nome</label>
<input type="text" name="name" id="name" value="{{ old('name', $district->name) }}">

<label for="district_type">Tipo</label>
<select name="district_type" id="district_type">
    <option value="urban">Urbano</option>
    <option value="rural">Rural</option>
</select>

<button class="btn btn-primary" type="submit">Salvar</button>
