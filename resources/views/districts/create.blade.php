@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Bairro</h2>
    <a href="/admin/districts">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('districts.store') }}">
            @include('districts/_form')
        </form>
    </div>
@stop