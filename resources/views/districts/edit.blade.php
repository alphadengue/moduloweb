@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Bairro</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('districts.update', [ $district->id ]) }}">
            {{ method_field('PUT') }}
            @include('districts._form')
        </form>
    </div>
@stop
