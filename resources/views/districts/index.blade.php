@extends('admin')

@section('content')
    <h2 class="dashboard-title">Bairros</h2>
    <a href="/admin/districts/create">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="GET" action="{{ route('districts.index') }}">
            @include('partials._search')
        </form>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Região</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($districts as $district)
                <tr>
                    <td>{{ $district->name }}</td>
                    <td>{{ $district->region->name }} ({{ $district->region->city->name }})</td>
                    <td>
                        <a href="{{ action('DistrictsController@show', $district->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('DistrictsController@edit', $district->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('DistrictsController@destroy', [ $district->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $districts->render() !!}
    </div>
@stop
