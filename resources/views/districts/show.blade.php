@extends('admin')

@section('content')
    <h2 class="dashboard-title">Bairro: {{ $district->name }}</h2>
    <div class="dash-content">

        <h3>Logradouros</h3>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($district->streets as $street)
                <tr>
                    <td>{{ $street->name }}</td>
                    <td>
                        <a href="{{ action('StreetsController@show', $street->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop