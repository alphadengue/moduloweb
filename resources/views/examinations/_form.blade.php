{{ csrf_field() }}

<label for="name">Descrição:</label>
<input type="text" name="name" id="name" value="{{ old('name', $examination->name) }}">

<button type="submit" class="btn btn-primary">Salvar</button>