@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Exame</h2>
    <a href="/admin/examinations">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('examinations.store') }}">
            @include('examinations._form')
        </form>
    </div>
@stop