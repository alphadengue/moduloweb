@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Exame</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('examinations.update', [ $examination->id ]) }}">
            {{ method_field('PUT') }}
            @include('examinations._form')
        </form>
    </div>
@stop