@extends('admin')

@section('content')
    <h2 class="dashboard-title">Exames</h2>
    <a href="/admin/examinations/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($examinations as $examination)
                <tr>
                    <td>{{ $examination->name }}</td>
                    <td>
                        <a href="{{ action('ExaminationsController@show', $examination->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('ExaminationsController@edit', $examination->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('ExaminationsController@destroy', [ $examination->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $examinations->render() }}
    </div>
@stop
