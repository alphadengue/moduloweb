@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Larva</h2>
    <a href="/admin/larvae">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('larvae.store') }}">
            @include('larvae._form')
        </form>
    </div>
@stop