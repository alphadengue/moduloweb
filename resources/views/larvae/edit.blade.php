@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Larva</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('larvae.update', [ $larva->id ]) }}">
            {{ method_field('PUT') }}
            @include('larvae._form')
        </form>
    </div>
@stop