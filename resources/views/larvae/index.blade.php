@extends('admin')

@section('content')
    <h2 class="dashboard-title">Larvas</h2>
    <a href="/admin/larvae/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach($larvae as $larva)
                    <tr>
                        <td>{{ $larva->id }}</td>
                        <td>{{ $larva->name }}</td>
                        <td>
                            <a href="{{ action('LarvaeController@show', $larva->id) }}" class="btn btn-primary btn-xs">
                                Ver
                            </a>
                            <a href="{{ action('LarvaeController@edit', $larva->id) }}" class="btn btn-info btn-xs">
                                Editar
                            </a>
                            <form method="POST"
                                  class="form--inline"
                                  action="{{ action('LarvaeController@destroy', [ $larva->id ]) }}"
                            >
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-xs">
                                    Excluir
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop
