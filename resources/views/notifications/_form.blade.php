{{ csrf_field() }}
<fieldset>
    <legend>Geral</legend>
    <div class="tile tile-6">
        <label for="code">Número Notificação</label>
        <input type="text" name="code" id="code" value="{{ old('code', $notification->code) }}">
    </div>
    <div class="tile tile-6">
        <label for="notification_date">Data Notificação</label>
        <input type="date" name="notification_date" id="notification_date" value="{{ old('notification_date', $notification->notification_date) }}">
    </div>
    <div class="tile tile-6">
        <label for="fist_signs_date">Data dos Primeiros Sintomas</label>
        <input type="date" name="first_signs_date" id="first_signs_date" value="{{ old('first_signs_date', $notification->first_signs_date) }}">
    </div>
    <div class="tile tile-9">
        <label for="notification_city_id">Município de Notificação</label>
        <select name="notification_city_id" id="notification_city_id">
            <option disabled selected>Selecione</option>
            @foreach ($all_cities as $id => $city)
                <option value="{{ $id }}"
                        @if ($id == old('notification_city_id', $notification->notification_city_id))
                        selected
                        @endif
                >
                    {{ $city }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-9">
        <label for="contamination_city_id">Provável município de infecção</label>
        <select name="contamination_city_id" id="contamination_city_id">
            <option disabled selected>Selecione</option>
            @foreach ($all_cities as $id => $city)
                <option value="{{ $id }}"
                        @if ($id == old('contamination_city_id', $notification->contamination_city_id))
                        selected
                        @endif
                >
                    {{ $city }}
                </option>
            @endforeach
        </select>
    </div>
</fieldset>
<fieldset>
    <legend>Paciente</legend>
    <div class="tile tile-12">
        <label for="patient_name">Nome do Paciente</label>
        <input type="text" name="patient_name" id="patient_name" value="{{ old('patient_name', $notification->patient_name) }}">
    </div>
    <div class="tile tile-3">
        <label for="birth_date">Nascimento</label>
        <input type="date" name="birth_date" id="birth_date" value="{{ old('birth_date', $notification->birth_date) }}">
    </div>
    <div class="tile tile-3">
        <label for="patient_age">Idade</label>
        <input type="text" name="patient_age" id="patient_age" disabled>
    </div>
    <div class="tile tile-18">
        <label for="patient_mother">Mãe do Paciente</label>
        <input type="text" name="patient_mother" id="patient_mother" value="{{ old('patient_mother', $notification->patient_mother) }}">
    </div>
</fieldset>
<fieldset>
    <legend>Residência</legend>
    <div class="tile tile-5">
        <label for="postcode">CEP</label>
        <input type="text"
               name="postcode"
               id="postcode"
               value="{{ old('postcode', $notification->building->street->postcode ?? null) }}"
        >
    </div>
    <div class="tile tile-5">
        <label for="state_id">Estado</label>
        <select name="state_id" id="state_id">
            <option disabled selected>Selecione</option>
            @foreach ($states as $id => $state)
                <option value="{{ $id }}"
                        @if ($id == old('state_id', $notification->building->street->district->region->city->state_id ?? null))
                        selected
                        @endif
                >
                    {{ $state }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-8">
        <label for="city_id">Cidade</label>
        <select name="city_id" id="city_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-5">
        <label for="region_id">Região</label>
        <select name="region_id" id="region_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-5">
        <label for="district_id">Bairro</label>
        <select name="district_id" id="district_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-8">
        <label for="street_id">Logradouro</label>
        <select name="street_id" id="street_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-11">
        <label for="building_id">Imóvel</label>
        <select name="building_id" id="building_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-7">
        <label for="patient_phone">Telefone Residencial</label>
        <input type="text" name="patient_phone" id="patient_phone">
    </div>
</fieldset>
<fieldset>
    <legend>Outros</legend>
    <div class="tile tile-6">
        <label for="examination_id">Exame</label>
        <select name="examination_id" id="examination_id">
            <option disabled selected>Selecione</option>
            @foreach ($examinations as $id => $examination)
                <option value="{{ $id }}"
                        @if ($id == old('examination_id', $notification->examination_id))
                        selected
                        @endif
                >
                    {{ $examination }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-6">
        <label for="result">Resultado</label>
        <select name="result" id="result">
            <option value="positive">Positivo</option>
            <option value="negative">Negativo</option>
            <option value="waiting">Aguardando</option>
        </select>
    </div>
    <div class="tile tile-6">
        <label for="closing_date">Data de Fechamento</label>
        <input type="date" name="closing_date" id="closing_date">
    </div>
    <div class="tile tile-18">
        <label for="observation">Observações</label>
        <textarea name="observation" id="observation" rows="5"></textarea>
    </div>
</fieldset>
<button type="submit" class="btn btn-primary pull-left">Salvar</button>