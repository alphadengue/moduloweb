@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Notificação</h2>
    @include('errors.list')
    <div class="dash-content">
        <form method="POST" action="{{ route('notifications.store') }}">
            @include('notifications._form')
        </form>
    </div>
@endsection