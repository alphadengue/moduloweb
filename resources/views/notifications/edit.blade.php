@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Notificação</h2>
    @include('errors.list')
    <div class="dash-content">
        <form method="POST" action="{{ route('notifications.update', [ $notification->id ]) }}">
            {{ method_field('PUT') }}
            @include('notifications._form')
        </form>
    </div>
@endsection