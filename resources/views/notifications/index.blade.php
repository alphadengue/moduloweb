@extends('admin')

@section('content')
    <h2 class="dashboard-title">Notificações</h2>
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
                <tr>
                    <th>Número</th>
                    <th>Paciente</th>
                    <th>Data da Notificação</th>
                    <th>Data dos Primeiros Sintomas</th>
                    <th>Reportada em</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            @foreach($notifications as $notification)
                <tr>
                    <td>{{ $notification->code }}</td>
                    <td>{{ $notification->patient_name }}</td>
                    <td>{{ \Carbon\Carbon::parse($notification->notification_date)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($notification->first_signs_date)->format('d/m/Y') }}</td>
                    <td>{{ \Carbon\Carbon::parse($notification->created_at)->format('d/m/Y H:i') }}</td>
                    <td>
                        <a href="{{ action('NotificationsController@show', $notification->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('NotificationsController@edit', $notification->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('NotificationsController@destroy', [ $notification->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $notifications->render() !!}
    </div>
@stop
