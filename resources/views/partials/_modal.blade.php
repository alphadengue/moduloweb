<div class="modal" tabindex="-1" role="dialog" aria-labelledby="modal-login" aria-hidden="true" id="login">
    <div class="modal-inner">
        <header class="modal-header" id="modal-login"><h2>Login</h2></header>
        <div class="modal-body">
            <div id="error-list"></div>
            <form method="POST" action="/login" class="formLogin" id="form-login">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <label for="field-user" class="form-label">Usuário</label>
                <input type="email" id="field-user" name="email" value="{{ old('email') }}" autofocus required>
                <label for="field-password" class="form-label">Senha</label>
                <input type="password" id="field-password" name="password" required>
                <div class="input-group">
                    <input type="checkbox" name="remember"> Manter conectado
                </div>
                <button type="submit" class="btn btn-success">Entrar</button>
            </form>
            <div id="loading">
                <p>Autenticando</p>
                <img src="/images/loading.gif" height="15" width="128" alt="">
            </div>
        </div>
        <footer class="modal-footer">
            <a href="/password/email">Esqueci a senha</a>
        </footer>
        <a href="#" class="btn-close">x</a>
    </div>
</div>