<div class="input-group">
    <label for="q">Busca</label>
    <input type="text" name="q" id="q" value="{{ request('q') }}">
    <button type="submit" class="btn btn-success">
        Buscar
    </button>
</div>