<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Alpha Dengue - Vigilância Epidemiológica de Taubaté</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Varela+Round%7COpen+Sans">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="icon" href="{{ asset('/favicon.ico')  }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="site public">
    <header class="header">
        <h1 class="brand">
            <a href="{{ url('/') }}" class="brand-link">
                <img src="{{ asset('/images/alpha.png') }}" alt="Alpha Dengue" class="brand-image">
                <span class="brand-description">ALPHA DENGUE</span>
            </a>
        </h1>
        <nav class="main-menu">
            <a href="#" id="menu-toggle">Menu</a>
            <ul class="menu-content">
                <li class="menu-item">
                    <a href="{{ url('/') }}" class="menu-link">Home</a>
                </li>
                <li class="menu-item">
                    <a href="{{ url('/vigilancia') }}" class="menu-link">Vigilância</a>
                </li>
                <li class="menu-item">
                    <a href="{{ url('/dengue') }}" class="menu-link">Dengue</a>
                </li>
                <li class="menu-item">
                    <a href="{{ url('/contato') }}" class="menu-link">Contato</a>
                </li>
                @if (auth()->check())
                    <li class="menu-item">
                        <a href="{{ url('/admin') }}" class="menu-link">Painel</a>
                    </li>
                @else
                    <li class="menu-item">
                        <a href="{{ url('/login') }}" class="menu-link">
                            Login
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
    </header>
    @yield('content')
    <footer class="footer">
        <p>
            Alpha Dengue - Vigilância Epidemiológica de Taubaté - Produzido por alunos da UNITAU.<br>
            Todos os direitos reservados - &copy; {{ date("Y") }}
        </p>
    </footer>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnEAcdFsRATyHvNghiOrxmFlOyQpixTdk&amp;libraries=geometry,visualization"></script>
    <script src="/js/main.js"></script>
</body>
</html>
