@extends('public')

@section('content')
    <div class="page">
        <h2 class="page-title">Contato</h2>
        @include('partials._feedback')
        @include('errors.list')
        <form method="POST" action="{{ route('contato') }}">
            {{ csrf_field() }}

            <label for="nome">Nome</label>
            <input type="text" name="nome" id="nome">

            <label for="email">Email</label>
            <input type="email" name="email" id="email">

            <label for="assunto">Assunto</label>
            <input type="text" name="assunto" id="assunto">

            <label for="mensagem">Mensagem</label>
            <textarea name="mensagem" id="mensagem" rows="10"></textarea>

            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@stop