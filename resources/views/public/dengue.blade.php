@extends('public')

@section('content')
    <div class="page">
        <h2 class="page-title">Todos contra a Dengue</h2>
        <h3 class="page-subtitle">Na luta contra a dengue nem você nem a água podem ficar parados.</h3>
        <img src="/images/dengue.jpg" alt="Mosquito da dengue" class="page-image" />
        <div class="content-text">
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
        </div>
    </div>
@stop