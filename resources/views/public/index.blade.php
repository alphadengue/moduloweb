@extends('public')

@section('content')
    <div class="home">
        <div class="heatmap" id="heatmap"></div>
        <div class="features">
            <div class="feature">
                <h2 class="feature-title">Dengue</h2>
                <img src="/images/dengue.jpg" class="feature-image" alt="Dengue" />
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea assumenda voluptate, iste repellendus neque quos nihil odit.</p>
            </div>
            <div class="feature">
                <h2 class="feature-title">A Vigilância</h2>
                <img src="/images/vigilancia.jpg" class="feature-image" alt="Vigilância" />
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea assumenda voluptate, iste repellendus neque quos nihil odit.</p>
            </div>
            <div class="feature">
                <h2 class="feature-title">Contato</h2>
                <img src="/images/contato.jpg" class="feature-image" alt="Contato" />
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea assumenda voluptate, iste repellendus neque quos nihil odit.</p>
            </div>
        </div>
    </div>
@stop