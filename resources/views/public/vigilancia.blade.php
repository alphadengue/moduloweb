@extends('public')

@section('content')
    <div class="page">
        <h2 class="page-title">Vigilância Epidemiológica</h2>
        <h3 class="page-subtitle">Cuidar é nossa profissão, levar a saúde é nossa obrigação!</h3>
        <img src="/images/vigilancia.jpg" alt="Vigilância Epidemiológica" class="page-image" />
        <div class="content-text">
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
            <p>
                Vigilância epidemiológica é um conjunto de ações que proporcionam o conhecimento, a detecção ou prevenção de qualquer mudança nos fatores determinantes e condicionantes de saúde individual ou coletiva, com a finalidade de recomendar e adotar as medidas de prevenção e controle das doenças ou agravos.
            </p>
            <p>
                Estratégias de prevenção e programas de controle específico de doenças requerem informações confiáveis sobre a situação dessas doenças ou seus antecedentes na população atendida. Sistemas de vigilância, portanto, são redes de pessoas e atividades que mantêm esse processo e podem funcionar em níveis locais e internacionais.
            </p>
        </div>
    </div>
@stop