{{ csrf_field() }}

<label for="state_id">Estado</label>
<select name="state_id" id="state_id">
    <option disabled selected>Selecione</option>
    @foreach ($states as $id => $state)
        <option value="{{ $id }}"
                @if ($id == old('state_id', $region->city->state_id ?? ''))
                selected
                @endif
        >
            {{ $state }}
        </option>
    @endforeach
</select>

<label for="city_id">Cidade</label>
<select name="city_id" id="city_id">
    <option disabled selected>Selecione</option>
    @foreach ($cities as $id => $city)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $region->city_id))
                selected
                @endif
        >
            {{ $city }}
        </option>
    @endforeach
</select>

<label for="name">Nome</label>
<input type="text" name="name" id="name" value="{{ old('name', $region->name) }}">

<button class="btn btn-primary" type="submit">Salvar</button>
