@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Região</h2>
    <a href="/admin/regions">Ver todas</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('regions.store') }}">
            @include('regions._form')
        </form>
    </div>
@stop
