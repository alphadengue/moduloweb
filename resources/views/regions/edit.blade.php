@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Região</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('regions.update', [ $region->id ]) }}">
            {{ method_field('PUT') }}
            @include('regions._form')
        </form>
    </div>
@stop
