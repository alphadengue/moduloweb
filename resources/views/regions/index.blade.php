@extends('admin')

@section('content')
    <h2 class="dashboard-title">Regiões</h2>
    <a href="/admin/regions/create">Adicionar nova</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="GET" action="{{ route('regions.index') }}">
            @include('partials._search')
        </form>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Cidade</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($regions as $region)
                <tr>
                    <td>{{ $region->name }}</td>
                    <td>{{ $region->city->name }}/{{ $region->city->state->abbreviation }}</td>
                    <td>
                        <a href="{{ action('RegionsController@show', $region->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('RegionsController@edit', $region->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('RegionsController@destroy', [ $region->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {!! $regions->render() !!}
    </div>
@stop
