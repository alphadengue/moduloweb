@extends('admin')

@section('content')
    <h2 class="dashboard-title">Região: {{ $region->name }}</h2>
    <div class="dash-content">

        <h3>Bairros</h3>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($region->districts as $district)
                <tr>
                    <td>{{ $district->name }}</td>
                    <td>
                        <a href="{{ action('DistrictsController@show', $district->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop