@extends('admin')

@section('content')
    <h2 class="dashboard-title">Relatórios</h2>

    <!-- SELECT BOX TO SELECT THE DATE RANGE -->

    <div class="report-box">
        <h3>Geral</h3>
        <div class="report-graph">
            <canvas id="general-chart" class="chart"></canvas>
        </div>
        <div class="report-table"></div>
    </div>

    <div class="report-box">
        <h3>Por faixa etária</h3>
        <div class="report-graph">
            <canvas id="age-chart" class="chart"></canvas>
        </div>
        <div class="report-table"></div>
    </div>

    <div class="report-box">
        <h3>Por região</h3>
        <div class="report-graph">
            <canvas id="region-chart" class="chart"></canvas>
        </div>
        <div class="report-table"></div>
    </div>
@stop