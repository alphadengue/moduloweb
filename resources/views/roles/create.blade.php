@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Cargo</h2>
    <a href="/admin/roles">Ver todos</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('roles.store') }}">
            @include('roles._form')
        </form>
    </div>
@stop