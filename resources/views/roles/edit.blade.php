@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Cargo</h2>
    <a href="/admin/roles">Ver todos</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('roles.update', [ $role->id ]) }}">
            {{ method_field('PATCH') }}
            @include('roles._form')
        </form>
    </div>
@stop