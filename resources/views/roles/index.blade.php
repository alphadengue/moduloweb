@extends('admin')

@section('content')
    <h2 class="dashboard-title">Cargos</h2>
    <a href="/admin/roles/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Cargo</th>
                <th>Usuários</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($roles as $role)
                <tr>
                    <td>{{ $role->name }}</td>
                    <td>{{ $role->users_count }}</td>
                    <td>
                        <a href="{{ action('RolesController@show', $role->id) }}"
                           class="btn btn-primary btn-xs"
                        >
                            Ver
                        </a>
                        <a href="{{ action('RolesController@edit', $role->id) }}"
                           class="btn btn-info btn-xs"
                        >
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('RolesController@destroy', [ $role->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $roles->links() }}
    </div>
@stop
