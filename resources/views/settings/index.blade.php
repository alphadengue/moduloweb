@extends('admin')

@section('content')
    <h2 class="dashboard-title">Configurações</h2>

    <form action="{{ url('admin/settings') }}" method="post">
        @foreach ($settings as $setting)
            <label for="{{ str_slug($setting->option) }}_field">{{ $setting->option }}</label>
            <input type="text" value="{{ $setting->value }}" id="{{ str_slug($setting->option) }}_field">
        @endforeach
    </form>
@stop