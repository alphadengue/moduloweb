{{ csrf_field() }}

<label for="name">Nome:</label>
<input type="text" name="name" id="name" value="{{ old('name', $state->name) }}" autofocus>

<label for="abbreviation">Sigla:</label>
<input type="text" name="abbreviation" id="abbreviation" value="{{ old('abbreviation', $state->abbreviation) }}">

<button type="submit" class="btn btn-primary">Salvar</button>
