@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Estado</h2>
    <a href="/admin/states">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('states.store') }}">
            @include('states._form')
        </form>
    </div>
@stop