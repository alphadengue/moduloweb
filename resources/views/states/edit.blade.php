@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Estado</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('states.update', [ $state->id ]) }}">
            {{ method_field('PUT') }}
            @include('states._form')
        </form>
    </div>
@stop