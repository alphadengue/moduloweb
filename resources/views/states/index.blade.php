@extends('admin')

@section('content')
    <h2 class="dashboard-title">Estados</h2>
    <a href="{{ route('states.create') }}">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="GET" action="{{ route('states.index') }}">
            @include('partials._search')
        </form>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Sigla</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($states as $state)
                <tr>
                    <td>{{ $state->name }}</td>
                    <td>{{ $state->abbreviation }}</td>
                    <td>
                        <a href="{{ action('StatesController@show', $state->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('StatesController@edit', $state->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('StatesController@destroy', [ $state->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $states->appends(request()->query())->links() }}
    </div>
@stop
