@extends('admin')

@section('content')
    <h2 class="dashboard-title">Estado: {{ $state->name }}</h2>
    <div class="dash-content">

        <h3>Municípios</h3>
        <table>
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Visualizar</th>
                </tr>
            </thead>
            <tbody>
                @foreach($state->cities as $city)
                    <tr>
                        <td>{{ $city->name }}</td>
                        <td>
                            <a href="{{ action('CitiesController@show', $city->id) }}" class="btn btn-primary btn-xs">
                                Ver
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@stop