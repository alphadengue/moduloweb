{{ csrf_field() }}

<label for="state_id">Estado</label>
<select name="state_id" id="state_id">
    <option disabled selected>Selecione</option>
    @foreach ($states as $id => $state)
        <option value="{{ $id }}"
                @if ($id == old('state_id', $street->district->region->city->state_id ?? ''))
                selected
                @endif
        >
            {{ $state }}
        </option>
    @endforeach
</select>

<label for="city_id">Cidade</label>
<select name="city_id" id="city_id">
    <option disabled selected>Selecione</option>
    @foreach ($cities as $id => $city)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $street->district->region->city_id ?? ''))
                selected
                @endif
        >
            {{ $city }}
        </option>
    @endforeach
</select>

<label for="region_id">Região</label>
<select name="region_id" id="region_id">
    <option disabled selected>Selecione</option>
    @foreach ($regions as $id => $region)
        <option value="{{ $id }}"
                @if ($id == old('city_id', $street->district->region_id))
                selected
                @endif
        >
            {{ $region }}
        </option>
    @endforeach
</select>

<label for="district_id">Bairro</label>
<select name="district_id" id="district_id">
    <option disabled selected>Selecione</option>
    @foreach ($districts as $id => $district)
        <option value="{{ $id }}"
                @if ($id == old('district_id', $street->district_id))
                selected
                @endif
        >
            {{ $district }}
        </option>
    @endforeach
</select>

<label for="postcode">CEP</label>
<input type="text" name="postcode" id="postcode" value="{{ old('postcode', $street->postcode) }}">

<label for="name">Nome:</label>
<input type="text" name="name" id="name" value="{{ old('name', $street->name) }}">

<button type="submit" class="btn btn-primary">Salvar</button>
