@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Logradouro</h2>
    <a href="/admin/streets">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('streets.store') }}">
            @include('streets._form')
        </form>
    </div>
@stop