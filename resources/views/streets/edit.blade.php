@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Logradouro</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('streets.update', [ $street->id ]) }}">
            {{ method_field('PUT') }}
            @include('streets._form')
        </form>
    </div>
@stop
