@extends('admin')

@section('content')
    <h2 class="dashboard-title">Logradouros</h2>
    <a href="/admin/streets/create">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="GET" action="{{ route('streets.index') }}">
            @include('partials._search')
        </form>
        <table>
            <thead>
            <tr>
                <th>Nome</th>
                <th>Bairro</th>
                <th>CEP</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($streets as $street)
                <tr>
                    <td>{{ $street->name }}</td>
                    <td>{{ $street->district->name }}</td>
                    <td>{{ preg_replace('/(\d{5})(\d{3})/', '$1-$2', $street->postcode) }}</td>
                    <td>
                        <a href="{{ action('StreetsController@show', $street->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('StreetsController@edit', $street->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('StreetsController@destroy', [ $street->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $streets->appends(request()->query())->links() }}
    </div>
@stop
