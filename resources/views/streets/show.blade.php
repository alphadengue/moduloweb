@extends('admin')

@section('content')
    <h2 class="dashboard-title">
        Logradouro: {{ $street->name }}
    </h2>

    @if ($street->postcode)
        <h3>
            CEP: {{ $street->postcode }}
        </h3>
    @endif

    <div class="dash-content">

        <h3>Imóveis</h3>
        <table>
            <thead>
            <tr>
                <th>Número</th>
                <th>Complemento</th>
                <th>Tipo de imóvel</th>
                <th>Visualizar</th>
            </tr>
            </thead>
            <tbody>
            @foreach($street->buildings as $building)
                <tr>
                    <td>{{ $building->street_number }}</td>
                    <td>{{ $building->additional_address_details }}</td>
                    <td>{{ $building->building_type->name }}</td>
                    <td>
                        <a href="{{ action('BuildingsController@show', $building->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop