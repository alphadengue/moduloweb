@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Tratamento</h2>
    <a href="/admin/treatments">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('treatments.store') }}">
            @include('treatments._form')
        </form>
    </div>
@stop