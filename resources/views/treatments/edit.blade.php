@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Tratamento</h2>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('treatments.update', $treatment->id) }}">
            {{ method_field('PATCH') }}
            @include('treatments._form')
        </form>
    </div>
@stop