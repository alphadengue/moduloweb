@extends('admin')

@section('content')
    <h2 class="dashboard-title">Tratamentos</h2>
    <a href="/admin/treatments/create">Adicionar novo</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>#</th>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($treatments as $treatment)
                <tr>
                    <td>{{ $treatment->id }}</td>
                    <td>{{ $treatment->name }}</td>
                    <td>
                        <a href="{{ action('TreatmentsController@show', $treatment->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('TreatmentsController@edit', $treatment->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('TreatmentsController@destroy', [ $treatment->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
