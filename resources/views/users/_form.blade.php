{{ csrf_field() }}

<label for="name">Nome:</label>
<input type="text" name="name" id="name" value="{{ old('name', $user->name) }}">

<label for="email">Email:</label>
<input type="email" name="email" id="email" value="{{ old('name', $user->email) }}">

<label for="password">Senha:</label>
<input type="password" name="password" id="password">

<label for="role_id">Função:</label>
<select name="role_id" id="role_id">
    <option disabled selected>Selecione</option>
    @foreach ($roles as $id => $role)
        <option value="{{ $id }}"
                @if ($id == old('role_id', $user->role_id))
                selected
                @endif
        >
            {{ $role }}
        </option>
    @endforeach
</select>

<button type="submit" class="btn btn-primary">
    Salvar
</button>