@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Usuário</h2>
    <a href="/admin/users">Ver todos</a>
    @include('errors.list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('users.store') }}">
            @include('users._form')
        </form>
    </div>
@stop