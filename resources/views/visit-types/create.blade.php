@extends('admin')

@section('content')
    <h2 class="dashboard-title">Novo Tipo de Visita</h2>
    <a href="/admin/visit-types">Ver todos</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <form method="POST" action="{{ route('visit-types.store') }}">
            @include('visit-types._form')
        </form>
    </div>
@stop