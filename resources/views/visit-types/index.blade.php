@extends('admin')

@section('content')
    <h2 class="dashboard-title">Tipos de Visita</h2>
    <a href="/admin/visit-types/create">Adicionar novo</a>
    @include('errors/list')
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Descrição</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($visit_types as $type)
                <tr>
                    <td>{{ $type->name }}</td>
                    <td>
                        <a href="{{ action('VisitTypesController@show', $type->id) }}" class="btn btn-primary btn-xs">
                            Ver
                        </a>
                        <a href="{{ action('VisitTypesController@edit', $type->id) }}" class="btn btn-info btn-xs">
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('VisitTypesController@destroy', [ $type->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
