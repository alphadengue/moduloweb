{{ csrf_field() }}

<fieldset>
    <legend>Geral</legend>
    <div class="tile tile-4">
        <label for="visit_type_id">Tipo de Visita</label>
        <select name="visit_type_id" id="visit_type_id">
            <option disabled selected>Selecione</option>
            @foreach ($visit_types as $id => $visit_type)
                <option value="{{ $id }}"
                        @if ($id == old('visit_type_id', $visit->visit_type_id))
                            selected
                        @endif
                >
                    {{ $visit_type }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-5">
        <label for="activity_id">Atividade</label>
        <select name="activity_id" id="activity_id">
            <option disabled selected>Selecione</option>
            @foreach ($activities as $id => $activity)
                <option value="{{ $id }}"
                        @if ($id == old('activity_id', $visit->activity_id))
                            selected
                        @endif
                >
                    {{ $activity }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-4">
        <label for="visited_at">Data da Visita</label>
        <input type="date"
               name="visited_at"
               id="visited_at"
               value="{{ old('visited_at', $visit->visited_at->format("Y-m-d")) }}"
        >
    </div>
    <div class="tile tile-5">
        <label for="agent_id">Agente</label>
        <select name="agent_id" id="agent_id">
            <option disabled selected>Selecione</option>
            @foreach ($agents as $id => $agent)
                <option value="{{ $id }}"
                        @if ($id == old('agent_id', $visit->agent_id))
                        selected
                        @endif
                >
                    {{ $agent }}
                </option>
            @endforeach
        </select>
    </div>
</fieldset>
<fieldset>
    <legend>Imóvel</legend>
    <div class="tile tile-5">
        <label for="postcode">CEP</label>
        <input type="text" name="postcode" id="postcode" value="{{ old('postcode') }}">
    </div>
    <div class="tile tile-5">
        <label for="state_id">Estado</label>
        <select name="state_id" id="state_id">
            <option disabled selected>Selecione</option>
            @foreach ($states as $id => $state)
                <option value="{{ $id }}"
                        @if ($id == old('state_id', $visit->building->street->district->region->city->state_id ?? ''))
                        selected
                        @endif
                >
                    {{ $state }}
                </option>
            @endforeach
        </select>
    </div>
    <div class="tile tile-8">
        <label for="city_id">Cidade</label>
        <select name="city_id" id="city_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-5">
        <label for="region_id">Região</label>
        <select name="region_id" id="region_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-5">
        <label for="district_id">Bairro</label>
        <select name="district_id" id="district_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-8">
        <label for="street_id">Logradouro</label>
        <select name="street_id" id="street_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-11">
        <label for="building_id">Imóvel</label>
        <select name="building_id" id="building_id">
            <option disabled selected>Selecione</option>
        </select>
    </div>
    <div class="tile tile-7">
        <label for="building_status_id">Situação do Imóvel</label>
        <select name="building_status_id" id="building_status_id">
            <option disabled selected>Selecione</option>
            @foreach ($building_statuses as $id => $building_status)
                <option value="{{ $id }}"
                        @if ($id == old('building_status_id'))
                        selected
                        @endif
                >
                    {{ $building_status }}
                </option>
            @endforeach
        </select>
    </div>
</fieldset>
<fieldset>
    <legend>Recipientes</legend>
    <table id="container-visit">
        <thead>
        <tr>
            <th class="table-label">Recipiente</th>
            <th class="table-label">Quantidade</th>
            <th class="table-label">Com Água</th>
            <th class="table-label">Com Larva</th>
            <th class="table-label">Tratamento</th>
            <th class="table-label">Amostra</th>
            <th class="table-label">Ações</th>
        </tr>
        </thead>
        <tbody id="container-row">
        <tr>
            <td class="container-column">
                <select name="container_id[]">
                    <option disabled selected>Selecione</option>
                    @foreach ($containers as $id => $container)
                        <option value="{{ $id }}">
                            {{ $container }}
                        </option>
                    @endforeach
                </select>
            </td>
            <td class="amount-column">
                <input type="number" name="amount[]">
            </td>
            <td class="amount-column">
                <input type="number" name="water_amount[]">
            </td>
            <td class="amount-column">
                <input type="number" name="larva_amount[]">
            </td>
            <td class="treatment-column">
                <select name="treatment_id[]">
                    <option disabled selected>Selecione</option>
                    @foreach ($treatments as $id => $treatment)
                        <option value="{{ $id }}">
                            {{ $treatment }}
                        </option>
                    @endforeach
                </select>
            </td>
            <td class="sample-column">
                <input type="text" name="sample[]">
            </td>
            <td class="submit-column"><button class="btn btn-block action-symbol" type="button" id="btn-add-container">+</button></td>
        </tr>
        <tr>
            <td class="container-column">
                <select name="container_id[]">
                    <option disabled selected>Selecione</option>
                    @foreach ($containers as $id => $container)
                        <option value="{{ $id }}">
                            {{ $container }}
                        </option>
                    @endforeach
                </select>
            </td>
            <td class="amount-column">
                <input type="number" name="amount[]">
            </td>
            <td class="amount-column">
                <input type="number" name="water_amount[]">
            </td>
            <td class="amount-column">
                <input type="number" name="larva_amount[]">
            </td>
            <td class="treatment-column">
                <select name="treatment_id[]">
                    <option disabled selected>Selecione</option>
                    @foreach ($treatments as $id => $treatment)
                        <option value="{{ $id }}">
                            {{ $treatment }}
                        </option>
                    @endforeach
                </select>
            </td>
            <td class="sample-column">
                <input type="text" name="sample[]">
            </td>
            <td class="submit-column"><button class="btn btn-block" type="button">-</button></td>
        </tr>
        </tbody>
    </table>
</fieldset>
<button type="submit" class="btn btn-primary">Salvar</button>
