@extends('admin')

@section('content')
    <h2 class="dashboard-title">Nova Visita</h2>
    @include('errors.list')
    <div class="dash-content">
        <form method="POST" action="{{ action('VisitsController@store') }}">
            @include('visits/_form')
        </form>
    </div>
@endsection