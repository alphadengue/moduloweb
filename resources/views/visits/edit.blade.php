@extends('admin')

@section('content')
    <h2 class="dashboard-title">Editar Visita</h2>
    @include('errors.list')
    <div class="dash-content">
        <form method="POST" action="{{ route('visits.update', [ $visit->id ]) }}">
            {{ method_field('PUT') }}
            @include('visits._form')
        </form>
    </div>
@endsection