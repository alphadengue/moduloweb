@extends('admin')

@section('content')
    <h2 class="dashboard-title">Visitas</h2>
    @include('partials._messages')
    <div class="dash-content">
        <table>
            <thead>
            <tr>
                <th>Data da Visita</th>
                <th>Imóvel</th>
                <th>Tipo de visita</th>
                <th>Atividade</th>
                <th>Ações</th>
            </tr>
            </thead>
            <tbody>
            @foreach($visits as $visit)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($visit->visited_at)->format("d/m/Y") }}</td>
                    <td>{{ $visit->building->street->name }}, {{ $visit->building->street_number }}</td>
                    <td>{{ $visit->visit_type->name }}</td>
                    <td>{{ $visit->activity->name }}</td>
                    <td>
                        <a href="{{ action('VisitsController@show', $visit->id) }}"
                           class="btn btn-primary btn-xs"
                        >
                            Ver
                        </a>
                        <a href="{{ action('VisitsController@edit', $visit->id) }}"
                           class="btn btn-info btn-xs"
                        >
                            Editar
                        </a>
                        <form method="POST"
                              class="form--inline"
                              action="{{ action('VisitsController@destroy', [ $visit->id ]) }}"
                        >
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs">
                                Excluir
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
