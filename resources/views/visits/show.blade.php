@extends('admin')

@section('content')
    <h2 class="dashboard-title">Visualizar Visita</h2>
    <div class="dash-content">
        <strong>Data de Visita:</strong>
        {{ $visit->visited_at->format("d/m/Y H:i:s") }} <br>
        <strong>Dados da Residência:</strong> <br>
        <strong>Endereço:</strong>
        {{ $visit->building->street->name }}, {{ $visit->building->street_number }},
        {{ $visit->building->street->district->name }},
        {{ $visit->building->street->district->region->name }},
        {{ $visit->building->street->district->region->city->name }} <br>
        <strong>Situação do Imóvel</strong>
        {{ $visit->building_status->name }} <br>
        <strong>Atividade executada:</strong>
        {{ $visit->activity->name }} <br>
        <strong>Tipo de Visita:</strong>
        {{ $visit->visit_type->name }} <br>
        <strong>Agente:</strong>
        {{ $visit->agent->name }} <br>
        <strong>Cadastrado por:</strong>
        {{ $visit->user->name }} <br><br>

        <strong>Recipientes:</strong> <br>
        @foreach($visit->containers as $container)
            {{ $container->name }}: <br>
            {{ $container->pivot->total_amount }} encontrados <br>
            {{ $container->pivot->water_amount }} com água <br>
            {{ $container->pivot->larva_amount }} com larva <br> <br>
        @endforeach
    </div>
@endsection