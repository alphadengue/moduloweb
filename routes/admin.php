<?php

Route::get('/', 'AdminController@index')->name('home');
Route::get('reports', 'ReportsController@index')->name('reports');

//User and Authentications related
Route::resource('roles', 'RolesController');
Route::resource('users', 'UsersController');

//Visit related
Route::resource('visits', 'VisitsController');
Route::resource('visit-types', 'VisitTypesController');
Route::resource('activities', 'ActivitiesController');
Route::resource('larvae', 'LarvaeController');
Route::resource('building-statuses', 'BuildingStatusesController');
Route::resource('containers', 'ContainersController');
Route::resource('treatments', 'TreatmentsController');

//Notification related
Route::get('notifications/status', 'ReportsController@notificationsByStatus');
Route::get('notifications/age', 'ReportsController@notificationsByAge');
Route::get('notifications/regions', 'ReportsController@notificationsByRegion');
Route::resource('notifications', 'NotificationsController');
Route::resource('examinations', 'ExaminationsController');

//Location related
//State
Route::get('states/{id}/cities', 'StatesController@cities');
Route::get('states/search', 'StatesController@search');
Route::resource('states', 'StatesController');
//City
Route::get('cities/{id}/regions', 'CitiesController@regions');
Route::get('cities/search', 'CitiesController@search');
Route::resource('cities', 'CitiesController');
//Region
Route::get('regions/{id}/districts', 'RegionsController@districts');
Route::get('regions/search', 'RegionsController@search');
Route::resource('regions', 'RegionsController');
//District
Route::get('districts/{id}/streets', 'DistrictsController@streets');
Route::get('districts/search', 'DistrictsController@search');
Route::resource('districts', 'DistrictsController');
//Streets
Route::get('streets/{id}/buildings', 'StreetsController@getBuildings');
Route::get('streets/search', 'StreetsController@search');
Route::resource('streets', 'StreetsController');
//Buildings
Route::get('buildings/search', 'BuildingsController@search');
Route::resource('buildings', 'BuildingsController');
Route::resource('building-types', 'BuildingTypesController');

//Settings
Route::get('settings', 'SettingsController@index');
Route::post('settings', 'SettingsController@store');