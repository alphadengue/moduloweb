<?php

/* Authentication related routes */
Route::post('login', 'LoginController@login')->name('login');

/* Single Model related routes */
Route::get('zipcode/{zipcode}', 'ZipcodeController@index')->name('zipcode');

/* Resource related routes */
Route::resource('states', 'StatesController');

Route::resource('visits', 'VisitsController');

Route::resource('buildings', 'BuildingsController');

Route::resource('activities', 'ActivitiesController', [
    'only' => [ 'index', 'show', 'store', 'update' ]
]);

Route::resource('containers', 'ContainersController', [
    'only' => [ 'index', 'show', 'store', 'update' ]
]);
