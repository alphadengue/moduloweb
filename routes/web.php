<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
$router->get('/', 'PublicController@index');
$router->get('vigilancia', 'PublicController@vigilancia');
$router->get('dengue', 'PublicController@dengue');
$router->get('contato', [ 'as' => 'contato', 'uses' => 'PublicController@getContato' ]);
$router->post('contato', 'PublicController@postContato');
$router->get('notifications/points', 'NotificationsController@positives');
$router->get('visits/points', 'VisitsController@getPoints');
$router->get('geocoding/{address}', 'ApiController@getGeocoding');

$router->auth();