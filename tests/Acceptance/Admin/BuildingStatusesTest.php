<?php

namespace Tests\Acceptance\Admin;

use AlphaDengue\User;
use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BuildingStatusesTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * A building status requires a non-empty name.
     *
     * @return void
     */
    public function testItMustHaveANameToBeCreated()
    {
        $this->actingAs(factory(User::class)->create());

        $this->visit('/admin/building-statuses/create');
        $this->type('', 'name');
        $this->press('Salvar');

        $this->seePageIs('/admin/building-statuses/create');
        $this->see('O campo nome é obrigatório.');
    }
}
