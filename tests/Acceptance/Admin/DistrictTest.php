<?php

namespace Tests\Acceptance\Admin;

use AlphaDengue\User;
use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DistrictTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * The district show route only accepts numerical ids on route.
     *
     * @return void
     */
    public function testDistrictShowRouteOnlyAcceptsNumericIdAttribute()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $this->get('admin/districts/something')
             ->seeStatusCode(404);
    }

    /**
     * It requires the user to be logged in to view a district.
     *
     * @return void
     */
    public function testRedirectToLoginWhenToViewDistrict()
    {
        $district = factory(\AlphaDengue\District::class)->create();

        $this->visit('admin/districts/' . $district->id)
             ->seePageIs('login');
    }

    /**
     * It requires the user to be logged in to view a district.
     *
     * @return void
     */
    public function testDistrictCanBeEdited()
    {
        $user = factory(User::class)->create();
        $district = factory(\AlphaDengue\District::class)->create();

        $this->actingAs($user);

        $this->visit("admin/districts/{$district->id}/edit");
    }
}
