<?php

namespace Tests\Acceptance\Admin;

use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class NotificationsTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testItCanBeCreated()
    {
        $user = factory(\AlphaDengue\User::class)->create();
        $city = factory(\AlphaDengue\City::class)->create();
        $building = factory(\AlphaDengue\Building::class)->create();
        $examination = factory(\AlphaDengue\Examination::class)->create();

        $this->actingAs($user);
        $this->post('admin/notifications', [
            'code' => '1',
            'notification_date' => '2016-11-15',
            'first_signs_date' => '2016-11-08',
            'notification_city_id' => $city->id,
            'contamination_city_id' => $city->id,
            'patient_name' => 'John Doe',
            'patient_phone' => '(12) 9999-9999',
            'closing_date' => '2016-11-22',
            'birth_date' => '1994-06-26',
            'patient_mother' => 'Jane Doe',
            'building_id' => $building->id,
            'examination_id' => $examination->id,
            'result' => 'negative',
        ]);

        $this->assertRedirectedTo('admin/notifications', [
            'message' => 'Notificação criada com sucesso!'
        ]);
    }
}
