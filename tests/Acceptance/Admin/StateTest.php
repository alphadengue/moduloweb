<?php

namespace Tests\Acceptance\Admin;

use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StateTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * A state can be created.
     *
     * @return void
     */
    public function testStateCanBeCreated()
    {
        $this->actingAs(factory(\AlphaDengue\User::class)->create());

        $this->visit('/admin/states/create');
        $this->type('Foo', 'name');
        $this->type('FO', 'abbreviation');
        $this->press('Salvar');

        $this->seePageIs('/admin/states');
        $this->see('Estado cadastrado com sucesso!');
        $this->seeInDatabase(
            'states', [
                'name' => 'Foo',
                'abbreviation' => 'FO'
            ]
        );
    }

    /**
     * A state abbreviation must have 2 characters.
     *
     * @return void
     */
    public function testStateAbbreviationMustBe2Length()
    {
        $this->actingAs(factory(\AlphaDengue\User::class)->create());

        $this->visit('/admin/states/create');
        $this->type('Foo', 'name');
        $this->type('FOO', 'abbreviation');
        $this->press('Salvar');

        $this->seePageIs('/admin/states/create');
        $this->see('O campo sigla deve ter 2 caracteres');
        $this->notSeeInDatabase(
            'states', [
                'name' => 'Foo',
                'abbreviation' => 'FOO'
            ]
        );
    }

    /**
     * It must show an alert box with "The name field is required" message.
     *
     * @return void
     */
    public function testStateMustHaveAName()
    {
        $this->actingAs(factory(\AlphaDengue\User::class)->create());

        $this->visit('/admin/states/create');
        $this->type('', 'name');
        $this->type('FO', 'abbreviation');
        $this->press('Salvar');

        $this->seePageIs('/admin/states/create');
        $this->see('O campo nome é obrigatório');
        $this->notSeeInDatabase(
            'states', [
                'name' => '',
                'abbreviation' => 'FO'
            ]
        );
    }
}
