<?php

namespace Tests\Acceptance\Api;

use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActivityTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * An activity can be created with a name.
     *
     * @return void
     */
    public function testActivityCanBeCreated()
    {
        $user = factory(\AlphaDengue\User::class)->create();

        $this->actingAs($user, 'api');

        $this->json('POST', 'api/v1/activities', [
            'name' => 'Atividade X',
        ]);

        $this->seeStatusCode(201);
        $this->seeJson([
            'name' => 'Atividade X',
        ]);
    }

    /**
     * An activity requires a name to be created.
     *
     * @return void
     */
    public function testActivityRequiresNameToCreate()
    {
        $user = factory(\AlphaDengue\User::class)->create();

        $this->actingAs($user, 'api');

        $this->json('POST', 'api/v1/activities', [
            'name' => '',
        ]);

        $this->seeStatusCode(422);
        $this->seeJson([
            'name' => ['O campo Nome é obrigatório.'],
        ]);
    }

    /**
     * The route to edit activity resource is disabled on api.
     *
     * @return void
     */
    public function testEditActivityRouteIsDisabled()
    {
        $user = factory(\AlphaDengue\User::class)->create();
        $activity = factory(\AlphaDengue\Activity::class)->create();

        $this->actingAs($user, 'api');

        $this->json('GET', 'api/v1/activities/' . $activity->id . '/edit');

        $this->seeStatusCode(404);
    }

    /**
     * The identified attribute on show activity
     *
     * @return void
     */
    public function testActivityIdentifierRequiresAnValidIdOnShowRoute()
    {
        $user = factory(\AlphaDengue\User::class)->create();

        $this->actingAs($user, 'api');

        $this->json('GET', 'api/v1/activities/letters');

        $this->seeStatusCode(404);
    }
}
