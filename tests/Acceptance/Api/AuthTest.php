<?php

namespace Tests\Acceptance\Api;

use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * Tests if the user can authenticate to the api.
     *
     * @return void
     */
    public function testAuthUser()
    {
        $user = factory(\AlphaDengue\User::class)->create();

        $this->json('POST', 'api/v1/login', [
            'email' => $user->email,
            'password' => 'secret'
        ]);

        $this->seeJsonEquals([
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'api_token' => $user->api_token,
            'role_id' => $user->role_id,
            'created_at' => $user->created_at->format('Y-m-d H:i:s'),
            'updated_at' => $user->updated_at->format('Y-m-d H:i:s'),
        ]);
    }
}
