<?php

namespace Tests\Acceptance\Site;

use AlphaDengue\User;
use Tests\BrowserKitTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PublicPagesTest extends BrowserKitTestCase
{
    use DatabaseTransactions;

    /**
     * Test if a "Painel" link is available when i am logged in the platform.
     *
     * @test
     *
     * @return void
     */
    public function user_can_go_to_panel_from_home_when_logged_in()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user)
            ->visit('/')
            ->click('Painel')
            ->seePageIs('/admin');
    }

    /**
     * @test
     */
    public function user_redirects_to_login_if_access_admin_while_not_logged()
    {
        $this->visit('/admin')
             ->seePageIs('/login');
    }

    /**
     * Test if I can go to login page when i am logged out.
     *
     * @test
     *
     * @return void
     */
    public function user_can_go_to_login_page_when_logged_out()
    {
        $this->visit('/')
            ->click('Login')
            ->seePageIs('/login');
    }

    /**
     * Test if a user can login from the login page.
     *
     * @test
     *
     * @return void
     */
    public function user_can_login_from_login_page()
    {
        $user = factory(User::class)->create();

        $this->visit('/login')
            ->type($user->email, 'email')
            ->type('secret', 'password')
            ->press('Entrar')
            ->seePageIs('/admin');
    }
}
